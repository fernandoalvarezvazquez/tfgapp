

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tfgapp/api/peticiones.dart';
import 'package:tfgapp/clases/Club.dart';
import 'package:tfgapp/pages/BonoDetail.dart';

import '../clases/BonoClases.dart';
import '../clases/Usuario.dart';

class BonosListPerfil extends StatefulWidget{

  static const String ROUTE = "/bonos-perfil";

  @override
  _BonosListPerfil createState() => _BonosListPerfil();
}

class _BonosListPerfil extends State<BonosListPerfil>{

  Future<Usuario> getCurrentUser() async{
    var peticiones = Peticiones();
    return await peticiones.getUser();
  }

  Future<List<BonoClases>> getBonos() async{
    var peticiones = Peticiones();
    return await peticiones.getBonos();
  }

  Future<Club> getClub(int id) async{
    var peticiones = Peticiones();
    return await peticiones.getClub(id);
  }

  Widget _bonoCard(BonoClases bono){
    return Card(
      child: ListTile(
        title: Text(bono.nombre),
        subtitle: FutureBuilder<Club>(
          future: getClub(bono.idClub),
          builder: (context, snapshot){
            if (snapshot.connectionState == ConnectionState.waiting) {
              return CircularProgressIndicator();
            } else if (snapshot.hasError) {
              return Text('Error: ${snapshot.error}');
            } else {
              Club? club = snapshot.data;
              return Text('${club?.nombre}');
            }
          },
        ),
        trailing: Text('${bono.precio}€', style: TextStyle(fontSize: 18),),
        onTap: (){
          Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => BonoDetail(bono: bono))
          );
        },
      ),
    );
  }

  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        title: Text('Bonos de clases'),
        backgroundColor: Colors.deepPurple,
      ),
      body: Padding(
        padding: const EdgeInsets.all(15),
        child: Column(
          children: [
            FutureBuilder<List<BonoClases>>(
                future: getBonos(),
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.waiting) {
                    return CircularProgressIndicator();
                  } else if (snapshot.hasError) {
                    return Text('Error: ${snapshot.error}');
                  } else {
                    final bonos = snapshot.data ?? [];
                    return FutureBuilder<Usuario>(
                        future: getCurrentUser(),
                        builder: (context, snapshotUser) {
                          if (snapshot.connectionState ==
                              ConnectionState.waiting) {
                            return CircularProgressIndicator();
                          } else if (snapshot.hasError) {
                            return Text('Error: ${snapshot.error}');
                          } else {
                            final user = snapshotUser.data;
                            final bonosFiltrados = bonos.where((bono) =>
                            bono.idComprador == user?.id).toList();
                            if (bonosFiltrados.length > 0) {
                              return Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: SingleChildScrollView(
                                    child: Column(
                                      children: [
                                        Column(
                                            children: bonosFiltrados.map((
                                                bono) => _bonoCard(bono))
                                                .toList()
                                        ),
                                        SizedBox(height: 10),
                                      ],
                                    )

                                ),
                              );
                            } else {
                              return Column(
                                children: [
                                  SizedBox(height: 50),
                                  Center(
                                    child: Text('Aun no se han comprado bonos',
                                      style: TextStyle(fontSize: 25),),
                                  ),
                                ],
                              );
                            }
                          }
                        }
                    );
                  }
                }
            ),
          ],
        ),
      ),
    );
  }
}