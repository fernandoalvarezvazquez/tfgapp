import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:tfgapp/api/peticiones.dart';
import 'package:tfgapp/clases/Pista.dart';
import 'package:tfgapp/clases/Usuario.dart';
import 'package:tfgapp/pages/MenuPrincipal.dart';
import 'package:tfgapp/pages/PaypalWebView/WebviewPaypal.dart';
import 'package:tfgapp/pages/Perfil.dart';

import '../clases/Club.dart';
import '../clases/Partido.dart';

class reservaDetail extends StatefulWidget{
  static const String ROUTE = "/reserva-detalle";
  final Pista pista;
  final double precio;
  final DateTime hora_ini;
  final DateTime hora_fin;
  final Future<Usuario> creador;

  reservaDetail({required this.pista, required this.precio, required this.hora_ini, required this.hora_fin, required this.creador});

  @override
  _reservaDetail createState() => _reservaDetail();
}

enum PaymentOption { pagarAhora, pagarClub }

class _reservaDetail extends State<reservaDetail>{

  PaymentOption? _selectedPaymentOption;
  late Usuario usuario;

  @override
  void initState() {
    super.initState();
    _selectedPaymentOption = PaymentOption.pagarAhora;
  }

  Future<void> reservar(String creador, DateTime hora_ini, DateTime hora_fin, int idPista, bool pagado) async{
    var peticiones = Peticiones();
    await peticiones.createPartido(context, creador, hora_ini, hora_fin, idPista, pagado);
  }

  Future<void> borrarReserva(int idPista, String hora) async{
    var peticiones = Peticiones();
    Partido partido = await peticiones.getPartidoPistaHora(idPista, hora);
    print(partido.id);
    await peticiones.deletePartido(partido.id);
  }

  Future<Club> getClub(int id) async{
    var peticiones = Peticiones();
    return await peticiones.getClub(id);
  }

  Future<String> iniciarPago(double precio) async{
    var peticiones = Peticiones();
    String paymentURL = await peticiones.inicarPago(precio);
    return paymentURL;
  }

  void snackbarPagoCompletado(BuildContext context) {
    final snackBar = SnackBar(
      content: Text('¡Pago completado con éxito!'),
      backgroundColor: Colors.green,
      duration: Duration(seconds: 3),
    );

    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  void snackbarPagoCancelado(BuildContext context) {
    final snackBar = SnackBar(
      content: Text('¡Pago cancelado. Anulando reserva!'),
      backgroundColor: Colors.red,
      duration: Duration(seconds: 3),
    );

    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        title: Text('Detalles de reserva'),
        backgroundColor: Colors.deepPurple,
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Card(
          elevation: 4.0,
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  FutureBuilder <Club>(
                      future: getClub(widget.pista.club),
                      builder: (context, snapshot){
                        if (snapshot.connectionState == ConnectionState.waiting) {
                          return CircularProgressIndicator(); // Muestra un indicador de carga mientras espera
                        } else if (snapshot.hasError) {
                          return Text('Error: ${snapshot.error}'); // Manejar errores
                        } else {
                          final club = snapshot.data!;
                          return ListTile(
                            leading: Icon(FontAwesomeIcons.building),
                            title: Text('Club: ${club.nombre}'),
                          );
                        }
                      }
                  ),
                  Divider(),
                  ListTile(
                    leading: Icon(Icons.sports_tennis),
                    title: Text('Pista: ${widget.pista.nombre}'),
                  ),
                  Divider(),
                  ListTile(
                    leading: Icon(Icons.calendar_today),
                    title: Text('Fecha: ${widget.hora_ini.day}/${widget.hora_ini.month}/${widget.hora_ini.year}'),
                  ),
                  Divider(),
                  ListTile(
                    leading: Icon(Icons.timer),
                    title: Text('Hora inicio: ${widget.hora_ini.hour}:${widget.hora_ini.minute}'),
                  ),
                  Divider(),
                  ListTile(
                    leading: Icon(Icons.timer_off),
                    title: Text('Hora fin: ${widget.hora_fin.hour}:${widget.hora_fin.minute}'),
                  ),
                  Divider(),
                  FutureBuilder<Usuario>(
                    future: widget.creador,
                    builder: (context, snapshot) {
                      if (snapshot.connectionState == ConnectionState.done) {
                        if (snapshot.hasError) {
                          return ListTile(
                            leading: Icon(Icons.error),
                            title: Text('Error al cargar el creador'),
                          );
                        } else if (snapshot.hasData) {
                          usuario = snapshot.data!;
                          return ListTile(
                            leading: Icon(Icons.person),
                            title: Text('Creador: ${usuario.login}'),
                          );
                        } else {
                          return ListTile(
                            leading: Icon(Icons.error),
                            title: Text('No se encontró creador'),
                          );
                        }
                      } else {
                        return Center(child: CircularProgressIndicator());
                      }
                    },
                  ),
                  Divider(),
                  SingleChildScrollView(
                    child: Column(
                      children: <Widget>[
                        RadioListTile<PaymentOption>(
                          title: const Text('Pagar pista completa'),
                          value: PaymentOption.pagarAhora,
                          groupValue: _selectedPaymentOption,
                          onChanged: (PaymentOption? value) {
                            setState(() {
                              _selectedPaymentOption = value;
                            });
                          },
                        ),
                        RadioListTile<PaymentOption>(
                          title: const Text('Pagar en el club'),
                          value: PaymentOption.pagarClub,
                          groupValue: _selectedPaymentOption,
                          onChanged: (PaymentOption? value) {
                            setState(() {
                              _selectedPaymentOption = value;
                            });
                          },
                        ),
                      ],
                    )
                  ),
                  //Spacer(),
                  SizedBox(height: 20),
                  Center(
                    child: ElevatedButton(
                      onPressed: () async {
                        if (_selectedPaymentOption == PaymentOption.pagarAhora) {
                          await reservar(usuario.login, widget.hora_ini, widget.hora_fin, widget.pista.id, true);
                          String paymentUrl = await iniciarPago(widget.precio);

                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => WebviewPaypal(URLinicial: paymentUrl)
                            ),
                          ).then((result) {
                            if(result == "completed"){
                              snackbarPagoCompletado(context);
                              Navigator.pushNamed(context, MenuPrincipal.ROUTE);
                            }
                            else if(result == "cancelled"){
                              String hora = DateFormat("yyyy-MM-ddTHH:mm:ss").format(widget.hora_ini);
                              borrarReserva(widget.pista.id, hora);
                              snackbarPagoCancelado(context);
                              Navigator.pushNamed(context, MenuPrincipal.ROUTE);
                            }
                          });
                        } else {
                          reservar(usuario.login, widget.hora_ini, widget.hora_fin, widget.pista.id, false);
                          Navigator.pushNamed(context, MenuPrincipal.ROUTE);
                        }
                      },
                      style: ElevatedButton.styleFrom(
                        backgroundColor: Colors.deepPurple,
                        foregroundColor: Colors.white,
                        padding: EdgeInsets.symmetric(horizontal: 30, vertical: 15),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                      ),
                      child: Text(_selectedPaymentOption == PaymentOption.pagarAhora
                          ? 'Pagar pista completa ${widget.precio} €'
                          : 'Pagar en el club',
                          style: TextStyle(fontSize: 16)
                      ),
                    ),
                  ),
                  SizedBox(height: 10),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
