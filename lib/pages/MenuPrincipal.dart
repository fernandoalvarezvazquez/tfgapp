import 'package:flutter/material.dart';
import 'package:tfgapp/api/peticiones.dart';
import 'package:tfgapp/clases/Usuario.dart';
import 'package:tfgapp/pages/BonosList.dart';
import 'package:tfgapp/pages/MostrarClubes.dart';
import 'package:tfgapp/pages/Perfil.dart';
import 'package:tfgapp/pages/reservarPista.dart';

import '../clases/Partido.dart';
import 'PartidoDetail.dart';

class MenuPrincipal extends StatefulWidget {
  static const String ROUTE = "/menu-principal";

  @override
  _MenuPrincipal createState() => _MenuPrincipal();
}

class _MenuPrincipal extends State<MenuPrincipal> {
  int _selectedIndex = 0;
  late Usuario user;

  @override
  void initState(){
    super.initState();
    getCurrentUser();
  }

  Future<Usuario> getCurrentUser() async{
    var peticiones = Peticiones();
    user = await peticiones.getUser();
    return await peticiones.getUser();
  }

  Future<Usuario> getUserByLogin(String login) async{
    var peticiones = Peticiones();
    return await peticiones.getUsuarioByLogin(login);
  }

  Future<List<Partido>> getPartidosJugados(int id) async{
    var peticiones = Peticiones();
    return await peticiones.getPartidosFuturosUser(id);
  }

  Widget _partidoCard(Partido partido) {
    DateTime fecha = DateTime.parse(partido.hora_ini);

    // Separar jugadores en dos listas
    List<String> pareja1 = [];
    List<String> pareja2 = [];

    for (int i = 0; i < partido.jugadores.length; i++) {
      if (i < 2) {
        pareja1.add(partido.jugadores[i]);
      } else {
        pareja2.add(partido.jugadores[i]);
      }
    }

    if (pareja1.length < 2){
      pareja1.add('vacio');
    }

    if(pareja2.length < 2){
      if (pareja2.length == 1)
        pareja2.add('vacio');
      else
        for(int i = 0; i<2; i++){
          pareja2.add('vacio');
        }
    }

    return Card(
      margin: EdgeInsets.symmetric(vertical: 10, horizontal: 15),
      child: Padding(
        padding: EdgeInsets.all(10),
        child: LimitedBox(
          maxHeight: 750,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text(
                "${fecha.day}/${fecha.month}/${fecha.year}",
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
              ),
              SizedBox(height: 10),
              if (pareja1.isNotEmpty)
                _generarFilaJugadores(pareja1),
              Divider(color: Colors.black,
                  thickness: 2.0,
                  height: 20.0
              ),
              if (pareja2.isNotEmpty)
                _generarFilaJugadores(pareja2),
              TextButton(
                  onPressed: () async{
                    final updatedPartido = await Navigator.push<Partido?>(
                      context,
                      MaterialPageRoute(
                        builder: (context) => PartidoDetail(partido: partido),
                      ),
                    );

                    if (updatedPartido != null) {
                      setState(() {
                        if(partido.id == updatedPartido.id){
                          partido = updatedPartido;
                        }
                      });
                    }
                  },
                  style: TextButton.styleFrom(
                    backgroundColor: Colors.deepPurple,
                    foregroundColor: Colors.white,
                    padding: EdgeInsets.symmetric(horizontal: 15, vertical: 5),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5.0),
                    ),

                  ),
                  child: Text('Ver Detalles')
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _generarFilaJugadores(List<String> jugadores) {

    return Row(
      children: [
        ...jugadores.map((jugador) => Container(
          width: 50,
          child: Column(
            children: [
              if (jugador != 'vacio')
                FutureBuilder<Usuario>(
                  future: getUserByLogin(jugador),
                  builder: (context, snapshot) {
                    if (snapshot.connectionState == ConnectionState.done) {
                      if (snapshot.hasError) {
                        return ListTile(
                          leading: Icon(Icons.error),
                          title: Text('Error al cargar el creador'),
                        );
                      } else if (snapshot.hasData) {
                        Usuario usuario = snapshot.data!;
                        if (usuario.fotoPerfil.isEmpty)
                          return Column(
                            children: [
                              Icon(Icons.person, color: Colors.black, size: 40.0),
                              Text(jugador),
                            ],
                          );
                        else return Column(
                          children: [
                            CircleAvatar(
                              radius: 20,
                              backgroundImage: NetworkImage('http://10.0.2.2:8080/api/usuarios/${usuario.id}/foto-perfil'),
                            ),
                            Text(jugador),
                          ],
                        );

                      } else {
                        return ListTile(
                          leading: Icon(Icons.error),
                          title: Text('No se encontró usuario'),
                        );
                      }
                    } else {
                      return Center(child: CircularProgressIndicator());
                    }
                  },
                )
              else
                Column(
                  children: [
                    CircleAvatar(
                      radius: 20,
                      child: Icon(Icons.add, color: Colors.white),
                      backgroundColor: Colors.deepPurple,
                    ),
                    Text(jugador),
                  ],
                )
            ],
          ),
        )).toList(),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.deepPurple,
        title: Text('Menú Principal'),
        leading: Container(),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            height: 425,
            child: GridView.count(
              crossAxisCount: 2,
              padding: EdgeInsets.all(16.0),
              childAspectRatio: 8.0 / 9.0,
              children: [
                _opcionMenu(context, 'Reservar Pista', Icons.sports_tennis, reservarPista.ROUTE),
                _opcionMenu(context, 'Ver Bonos', Icons.card_giftcard, BonosList.ROUTE),
                _opcionMenu(context, 'Buscar por Club', Icons.location_city, MostrarClubes.ROUTE),
                _opcionMenu(context, 'Mis Clubes', Icons.favorite, MostrarClubes.ROUTE),
              ],
            ),
          ),
          Expanded(
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(height: 20),
                    Padding(
                      padding: const EdgeInsets.only(left: 15),
                      child: Text("Mis partidos futuros:", style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
                    ),

                    FutureBuilder <Usuario>(
                        future: getCurrentUser(),
                        builder: (context, snapshotUser){
                          if (snapshotUser.connectionState == ConnectionState.waiting) {
                            return CircularProgressIndicator();
                          } else if (snapshotUser.hasError) {
                            return Text("Error: ${snapshotUser.error}");
                          } else {
                            Usuario user = snapshotUser.data!;
                            return FutureBuilder <List<Partido>>(
                              future: getPartidosJugados(user.id),
                              builder: (context, snapshot){
                                if (snapshot.connectionState == ConnectionState.waiting) {
                                  return CircularProgressIndicator();
                                } else if (snapshot.hasError) {
                                  return Text("Error: ${snapshot.error}");
                                } else {
                                  List<Partido> partidos = snapshot.data!;
                                  if (partidos.isNotEmpty)
                                    return Container(
                                        height: 250,
                                        child: ListView.builder(
                                          scrollDirection: Axis.horizontal,
                                          itemCount: partidos?.length ?? 0,
                                          itemBuilder: (context, index) {
                                            return _partidoCard(partidos[index]);
                                          },
                                        ),
                                    );
                                  else return Padding(
                                      padding: const EdgeInsets.all(30),
                                      child: Center(child: Text('No tienes partidos futuros'))
                                );
                                }
                              },
                            );
                          }
                        }
                    ),
                  ],
                ),
              ),
          ),
          _menuInferior(),
        ],
      ),
    );
  }

  Widget _opcionMenu(BuildContext context, String titulo, IconData icono, String ruta) {
    return Card(
      child: InkWell(
        onTap: () {
          Navigator.pushNamed(context, ruta);
        },
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(icono, size: 40.0),
            Text(titulo, textAlign: TextAlign.center),
          ],
        ),
      ),
    );
  }

  Widget _menuInferior() {
    return BottomNavigationBar(
      items: const <BottomNavigationBarItem>[
        BottomNavigationBarItem(
          icon: Icon(Icons.sports_tennis),
          label: 'Jugar',
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.school),
          label: 'Clases',
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.person),
          label: 'Perfil',
        ),
      ],
      currentIndex: _selectedIndex,
      onTap: (index) {
        setState(() {
          _selectedIndex = index;
        });
        switch (_selectedIndex) {
          case 0:
            Navigator.pushNamed(context, MenuPrincipal.ROUTE);
            break;
          case 1:
            Navigator.pushNamed(context, BonosList.ROUTE);
            break;
          case 2:
            Navigator.pushNamed(context, Perfil.ROUTE);
            break;
        }
      },
    );
  }
}
