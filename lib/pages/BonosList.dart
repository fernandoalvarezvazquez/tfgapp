

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tfgapp/api/peticiones.dart';
import 'package:tfgapp/clases/Club.dart';
import 'package:tfgapp/pages/BonoDetail.dart';

import '../clases/BonoClases.dart';
import 'MenuPrincipal.dart';
import 'Perfil.dart';

class BonosList extends StatefulWidget{

  static const String ROUTE = "/bonos";

  @override
  _BonosList createState() => _BonosList();
}

class _BonosList extends State<BonosList>{

  int _selectedIndex = 1;

  Future<List<BonoClases>> getBonos() async{
    var peticiones = Peticiones();
    return await peticiones.getBonos();
  }

  Future<Club> getClub(int id) async{
    var peticiones = Peticiones();
    return await peticiones.getClub(id);
  }

  Widget _bonoCard(BonoClases bono){
    return Card(
      child: ListTile(
          title: Text(bono.nombre),
          subtitle: FutureBuilder<Club>(
            future: getClub(bono.idClub),
            builder: (context, snapshot){
              if (snapshot.connectionState == ConnectionState.waiting) {
                return CircularProgressIndicator();
              } else if (snapshot.hasError) {
                return Text('Error: ${snapshot.error}');
              } else {
                Club? club = snapshot.data;
                return Text('${club?.nombre}');
              }
            },
          ),
          trailing: Text('${bono.precio}€', style: TextStyle(fontSize: 18),),
        onTap: (){
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => BonoDetail(bono: bono))
          );
        },
      ),
    );
  }

  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        title: Text('Bonos de clases'),
        backgroundColor: Colors.deepPurple,
      ),
      body:Column(
        children: [
          Expanded(
            child:  Padding(
              padding: const EdgeInsets.all(15),
              child: FutureBuilder<List<BonoClases>>(
              future: getBonos(),
              builder: (context, snapshot){
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return CircularProgressIndicator();
                } else if (snapshot.hasError) {
                  return Text('Error: ${snapshot.error}');
                } else {
                  final bonos = snapshot.data ?? [];
                  final bonosFiltrados = bonos.where((bono) => bono.idComprador == 0).toList();
                  if(bonosFiltrados.length > 0){
                    return Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: SingleChildScrollView(
                          child: Column(
                            children: [
                              Column(
                                  children: bonosFiltrados.map((bono) => _bonoCard(bono)).toList()
                              ),
                              SizedBox(height: 10),
                            ],
                          )

                      ),
                    );
                  } else {
                    return Column(
                      children: [
                        SizedBox(height: 50),
                        Expanded(
                          child: Center(
                            child: Text('Aun no hay bonos disponibles', style: TextStyle(fontSize: 25)),
                          ),
                        )
                      ],
                    );
                  }

                }
              }
          ),
            ),
          ),
          _menuInferior(),
        ],
      ),
    );
  }
  Widget _menuInferior() {
    return BottomNavigationBar(
      items: const <BottomNavigationBarItem>[
        BottomNavigationBarItem(
          icon: Icon(Icons.sports_tennis),
          label: 'Jugar',
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.school),
          label: 'Clases',
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.person),
          label: 'Perfil',
        ),
      ],
      currentIndex: _selectedIndex,
      onTap: (index) {
        setState(() {
          _selectedIndex = index;
        });
        switch (_selectedIndex) {
          case 0:
            Navigator.pushNamed(context, MenuPrincipal.ROUTE);
            break;
          case 1:
            Navigator.pushNamed(context, BonosList.ROUTE);
            break;
          case 2:
            Navigator.pushNamed(context, Perfil.ROUTE);
            break;
        }
      },
    );
  }

}