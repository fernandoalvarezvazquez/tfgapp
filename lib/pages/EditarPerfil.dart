import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../api/peticiones.dart';
import '../clases/Usuario.dart';

class EditarPerfil extends StatefulWidget {
  static const String ROUTE = "/edit-perfil";

  @override
  _EditarPerfil createState() => _EditarPerfil();
}

class _EditarPerfil extends State<EditarPerfil> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController _loginController = TextEditingController();
  late Future<Usuario> user;
  late Usuario usuario;

  @override
  void initState() {
    super.initState();
    getCurrentUser();
  }

  Future<Usuario> getCurrentUser() async{
    var peticiones = Peticiones();
    user = peticiones.getUser();
    usuario = await peticiones.getUser();
    return await user;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.deepPurple,
        title: Text('Editar perfil'),
      ),
      body: FutureBuilder<Usuario>(
        future: user,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return CircularProgressIndicator();
          } else if (snapshot.hasError) {
            return Center(child: Text('Ha ocurrido un error.'));
          } else if (snapshot.hasData) {
            usuario = snapshot.data!;
            return Form(
              key: _formKey,
              child: ListView(
                padding: EdgeInsets.all(16.0),
                children: [
                  SizedBox(height: 20),
                  TextFormField(
                    controller: _loginController,
                    decoration: InputDecoration(labelText: 'Nombre de usuario'),
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Por favor ingresa tu nombre de usuario';
                      }
                      return null;
                    },
                  ),
                  SizedBox(height: 20),
                  ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      backgroundColor: Colors.deepPurple, // Color de fondo
                      foregroundColor: Colors.white, // Color del texto
                      padding: EdgeInsets.symmetric(horizontal: 30, vertical: 15),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                    ),
                    onPressed: _saveProfile,
                    child: Text('Guardar cambios'),
                  ),
                ],
              ),
            );
          } else {
            return Center(child: Text('No hay datos disponibles.'));
          }
        },
      ),
    );
  }

  void _saveProfile() {
    if (_formKey.currentState!.validate()) {
      setState(() {
        usuario.login = _loginController.text;
        // Aquí también puedes actualizar los otros campos
      });

      // Luego, puedes llamar a una API para guardar los cambios en tu servidor
      var peticiones = Peticiones();
      peticiones.updateUser(usuario.id, usuario);

      peticiones.autenticar(usuario.login, usuario.contrasena!);

      // Finalmente, puedes redirigir al usuario a la página de perfil o mostrar un mensaje de éxito
      Navigator.pop(context);
    }
  }
}
