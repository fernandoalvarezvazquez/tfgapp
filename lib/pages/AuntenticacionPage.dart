import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:tfgapp/api/peticiones.dart';
import '../clases/Usuario.dart';
import 'MenuPrincipal.dart';

class AutenticacionPage extends StatefulWidget {
  static const String  ROUTE= "/autenticacion";
  @override
  _AutenticacionPage createState() => _AutenticacionPage();
}

class _AutenticacionPage extends State<AutenticacionPage> {
  final _formKey = GlobalKey<FormState>();
  String _login = '';
  String _password = '';
  final Peticiones peticiones = Peticiones();
  final storage = FlutterSecureStorage();

  void _submit() async{
    if (_formKey.currentState!.validate()) {
      _formKey.currentState!.save();
      try {
        String token = await peticiones.autenticar(_login, _password);

        Usuario user = await peticiones.getUser();

        if (user.authority == 'PLAYER') {
          await storage.write(key: 'token', value: token);
          Navigator.pushNamed(
            context,
            MenuPrincipal.ROUTE,
          );
        } else {
          ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(
              content: Text('Solo los jugadores pueden acceder a la app movil.'),
              backgroundColor: Colors.red,
              duration: Duration(seconds: 3),
            ),
          );
        }
      } catch (error) {
        ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(content: Text(error.toString()), backgroundColor: Colors.red)
        );
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.deepPurple,
        title: Text('Autenticación'),
      ),
      body: Padding(
        padding: EdgeInsets.all(16.0),
        child: Form(
          key: _formKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              TextFormField(
                decoration: InputDecoration(labelText: 'Login'),
                validator: (value) {
                  if (value == null || value.isEmpty ) {
                    return 'Por favor ingrese un nombre de usuario';
                  }
                  return null;
                },
                onSaved: (value) {
                  _login = value!;
                },
              ),
              TextFormField(
                decoration: InputDecoration(labelText: 'Contraseña'),
                obscureText: true,
                validator: (value) {
                  if (value == null || value.isEmpty || value.length < 4) {
                    return 'La contraseña debe tener al menos 4 caracteres';
                  }
                  return null;
                },
                onSaved: (value) {
                  _password = value!;
                },
              ),
              SizedBox(
                height: 20,
              ),
              /*Text('Si aún no tienes cuenta, puedes REGISTRARTE AQUI'),
              SizedBox(
                height: 20,
              ),*/
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                  backgroundColor: Colors.deepPurple, // Color de fondo
                  foregroundColor: Colors.white, // Color del texto
                  padding: EdgeInsets.symmetric(horizontal: 30, vertical: 15),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                ),
                onPressed: _submit,
                child: Text('Iniciar Sesión'),
              ),
            ],
          ),
        ),
      ),
    );
  }
}