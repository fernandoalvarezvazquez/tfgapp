import 'package:flutter/material.dart';
import '../api/peticiones.dart';
import '../clases/Club.dart';
import 'clubDetail.dart';

late Club club;

class MostrarClubes extends StatefulWidget{
  static const String ROUTE = "/mostrar-clubes";

  @override
  _MostrarClubes createState() => _MostrarClubes();
}

class _MostrarClubes extends State<MostrarClubes>{

  late Future <List<Club>> listaClubes;
  List<Club> clubes = [];
  List<Club> clubesFiltrados = [];
  final TextEditingController _buscadorController = TextEditingController();

  @override
  void initState(){
    super.initState();
    obtenerClubes().then((result){
      clubes=result;
      clubesFiltrados = clubes;
      setState(() {
      });
    });
  }

  Future<List<Club>> obtenerClubes() async {
    var peticiones = Peticiones();
    listaClubes = peticiones.clubes();
    return await peticiones.clubes();
  }

  @override
  Widget build(BuildContext context){
    obtenerClubes();

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.deepPurple,
        title: Text('Seleccione un club'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(15),
        child: Column(
          children: [
            TextField(
              controller: _buscadorController,
              decoration: InputDecoration(
                labelText: "Buscar",
                hintText: "Buscar Club",
                prefixIcon: Icon(Icons.search),
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(25.0))
                ),
              ),
              onChanged: (value){
                setState(() {
                  clubesFiltrados = clubes.where((club) => club.nombre.toLowerCase().contains(value.toLowerCase())).toList();
                });
              },
            ),
            Expanded(
              child: ListView.builder(
                itemCount: clubesFiltrados.length,
                itemBuilder: (context, position){
                  return Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Center(
                      child: Card(
                        child: ListTile(
                          leading: Icon(Icons.sports_tennis),
                          title: Text(clubesFiltrados[position].nombre),
                          subtitle: Text(clubesFiltrados[position].direccion),
                          onTap: (){
                            club = clubesFiltrados[position];
                            Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => clubDetail(club: club)),
                            );
                          },
                        ),
                      ),
                    ),
                  );
                }
              ),
            ),
          ],
        ),
      ),
    );
  }
}