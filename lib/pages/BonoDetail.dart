import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tfgapp/clases/BonoClases.dart';
import 'package:tfgapp/clases/Usuario.dart';

import '../api/peticiones.dart';
import '../clases/Club.dart';
import 'MenuPrincipal.dart';
import 'PaypalWebView/WebviewPaypal.dart';

class BonoDetail extends StatefulWidget{

  static const String ROUTE = "/ver-bono";
  final BonoClases bono;

  BonoDetail({required this.bono});

  @override
  _BonoDetail createState() => _BonoDetail();
}

class _BonoDetail extends State<BonoDetail>{

  Future<Usuario> getCurrentUser() async{
    var peticiones = Peticiones();
    return await peticiones.getUser();
  }

  Future<Club> getClub(int id) async{
    var peticiones = Peticiones();
    return await peticiones.getClub(id);
  }

  Future<void> updateBonoPagado(BonoClases bono, int idJugador) async{
    var peticiones = Peticiones();
    await peticiones.updateBonoPagado(bono, idJugador);
  }

  Future<String> iniciarPago(double precio) async{
    var peticiones = Peticiones();
    String paymentURL = await peticiones.inicarPago(precio);
    return paymentURL;
  }

  void snackbarPagoCompletado(BuildContext context) {
    final snackBar = SnackBar(
      content: Text('¡Pago completado con éxito!'),
      backgroundColor: Colors.green,
      duration: Duration(seconds: 3),
    );

    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        title: Text('Ver bono'),
        backgroundColor: Colors.deepPurple,
      ),
      body: Padding(
        padding: const EdgeInsets.all(15),
        child: Column(
          children: [
            Card(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Center(child: Text('Informacion', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold))),
                  SizedBox(height: 10,),
                  Row(
                    children: [
                      SizedBox(width: 10,),
                      Icon(Icons.bookmark_border),
                      SizedBox(width: 10,),
                      Text(widget.bono.nombre, style: TextStyle(fontSize: 18),),
                    ],
                  ),
                  SizedBox(height: 10,),
                  Row(
                    children: [
                      SizedBox(width: 10),
                      Icon(Icons.info_outline),
                      SizedBox(width: 10,),
                      Flexible(
                        child: Text(
                          widget.bono.descripcion,
                          style: TextStyle(fontSize: 18),
                          overflow: TextOverflow.ellipsis,
                          maxLines: 3,
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 10,),
                  Row(
                    children: [
                      SizedBox(width: 10,),
                      Icon(Icons.location_on_outlined),
                      SizedBox(width: 10,),
                      FutureBuilder<Club>(
                        future: getClub(widget.bono.idClub),
                        builder: (context, snapshot){
                          if (snapshot.connectionState == ConnectionState.waiting) {
                            return CircularProgressIndicator();
                          } else if (snapshot.hasError) {
                            return Text('Error: ${snapshot.error}');
                          } else {
                            Club? club = snapshot.data;
                            return Text('${club?.nombre}', style: TextStyle(fontSize: 18));
                          }
                        },
                      ),
                    ],
                  ),
                  SizedBox(height: 10,),
                  Row(
                    children: [
                      SizedBox(width: 10,),
                      Icon(Icons.sports_tennis),
                      SizedBox(width: 10,),
                      Text('Numero de clases: ${widget.bono.num_clases}', style: TextStyle(fontSize: 18),)
                    ],
                  ),
                  SizedBox(height: 10,),
                  Row(
                    children: [
                      SizedBox(width: 10,),
                      Icon(Icons.wallet),
                      SizedBox(width: 10,),
                      Text('Precio: ${widget.bono.precio}€', style: TextStyle(fontSize: 18),)
                    ],
                  ),
                  SizedBox(height: 10,),
                ],
              )
            ),
            SizedBox(height: 20,),
            FutureBuilder<Usuario>(
              future: getCurrentUser(),
                builder: (context, snapshot){
                  if (snapshot.connectionState == ConnectionState.waiting) {
                    return CircularProgressIndicator();
                  } else if (snapshot.hasError) {
                    return Text('Error: ${snapshot.error}');
                  } else {
                    final usuario = snapshot.data!;
                    if(widget.bono.idComprador == 0)
                      return  ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          backgroundColor: Colors.deepPurple, // Color de fondo
                          foregroundColor: Colors.white, // Color del texto
                          padding: EdgeInsets.symmetric(horizontal: 30, vertical: 15),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                        ),
                        onPressed: () async{
                          String paymentUrl = await iniciarPago(widget.bono.precio);
                          updateBonoPagado(widget.bono, usuario.id);
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => WebviewPaypal(URLinicial: paymentUrl)
                            ),
                          ).then((result) {
                            if(result == "completed"){

                              snackbarPagoCompletado(context);
                              Navigator.pushNamed(context, MenuPrincipal.ROUTE);
                            }
                            else if(result == "cancelled"){

                            }
                          });
                        },
                        child: Text('Pagar')
                    );
                    else return SizedBox.shrink();
                  }
                  //return SizedBox.shrink();
                }
            ),

          ],
        ),
      ),
    );
  }
}