import 'package:flutter/material.dart';
import 'package:tfgapp/clases/Usuario.dart';
import 'package:tfgapp/pages/Perfil.dart';
import 'package:tfgapp/pages/TarjetaForm.dart';

import '../api/peticiones.dart';
import '../clases/Tarjeta.dart';

class TarjetaList extends StatefulWidget{

  static const String ROUTE = "/tarjeta-list";

  @override
  _TarjetaList createState() => _TarjetaList();
}

class _TarjetaList extends State<TarjetaList>{

  late Usuario jugador;

  @override initState(){
    super.initState();
    getCurrentUser();
  }

  Future<Usuario> getCurrentUser() async{
    var peticiones = Peticiones();
    jugador = await peticiones.getUser();
    return await peticiones.getUser();
  }

  Future<List<Tarjeta>> getTarjetasUser() async{
    var peticiones = Peticiones();
    return await peticiones.getTarjetas(jugador.id);
  }

  Future<void> deleteTarjeta(int id) async{
    var peticiones = Peticiones();
    await peticiones.deleteTarjeta(id);
  }

  Widget _tarjetaCard(Tarjeta tarjeta){
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10.0),
      child: Card(
        elevation: 5.0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Row(
            children: [
              Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Icon(Icons.credit_card, color: Colors.deepPurple), // Ícono de tarjeta
                          SizedBox(width: 10.0),
                          Text(
                            '**** **** **** ' + tarjeta.numero.substring(tarjeta.numero.length - 4), // Solo mostramos los últimos 4 dígitos para la privacidad
                            style: TextStyle(
                              fontSize: 18.0,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      ),
                      SizedBox(height: 20.0),
                      Text(
                        'Caduca: ' + tarjeta.caducidad,
                        style: TextStyle(
                          fontSize: 16.0,
                          color: Colors.grey[700],
                        ),
                      ),
                      // Puedes agregar más detalles de la tarjeta aquí, si lo deseas.
                    ],
                  ),
              ),
              SizedBox(width: 10.0),
              IconButton(
                icon: Icon(Icons.delete, color: Colors.red), // Ícono de basura
                onPressed: () {
                  deleteTarjeta(tarjeta.id);
                  setState(() {});
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  Widget build (BuildContext context){
    return Scaffold(
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            ListTile(
              leading: Icon(Icons.arrow_back),
              title: Text('Ir a página específica'),
              onTap: () {
                Navigator.pushNamed(context, Perfil.ROUTE);
              },
            ),
          ],
        ),
      ),
      appBar: AppBar(
        title: Text('Mis tarjetas'),
        backgroundColor: Colors.deepPurple,
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.pushNamed(context, Perfil.ROUTE);
          },
        ),
      ),
      body: Column(
        children: [
          SizedBox(height: 25),
          FutureBuilder<Usuario>(
            future: getCurrentUser(),
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                return CircularProgressIndicator();
              } else if (snapshot.hasError) {
                return Text('Error: ${snapshot.error}');
              } else {
                jugador = snapshot.data!;
                return FutureBuilder<List<Tarjeta>>(
                    future: getTarjetasUser(),
                    builder: (context, snapshot) {
                      if (snapshot.connectionState == ConnectionState.waiting) {
                        return CircularProgressIndicator();
                      } else if (snapshot.hasError) {
                        return Text('Error: ${snapshot.error}');
                      } else {
                        final tarjetas = snapshot.data ?? [];
                        return Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: SingleChildScrollView(
                              child: Column(
                                children: [
                                  Text('Mis tarjetas:', style: TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold),),
                                  Column(
                                    children: tarjetas.map((tarjeta) =>
                                        _tarjetaCard(tarjeta)).toList(),
                                  ),
                                ],
                              )
                          ),
                        );
                      }
                    }
                );
              }
            }
            ),
          SizedBox(height: 10),
          Center(
            child: Card(
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  backgroundColor: Colors.white,
                  foregroundColor: Colors.black,
                  padding: EdgeInsets.symmetric(horizontal: 30, vertical: 15),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                ),
                onPressed: () {
                  Navigator.pushNamed(context, TarjetaForm.ROUTE);
                },
                child: Row(
                  mainAxisSize: MainAxisSize.min, // para que el Row no sea tan ancho como el posible
                  children: <Widget>[
                    Icon(Icons.add), // Icono que desees usar
                    SizedBox(width: 8), // Espacio entre el icono y el texto
                    Text('Agregar Tarjeta'), // Texto del botón
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}