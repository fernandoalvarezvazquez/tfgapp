import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:tfgapp/api/peticiones.dart';
import 'package:tfgapp/clases/Club.dart';
import 'package:tfgapp/pages/AddResultado.dart';
import 'package:tfgapp/pages/MenuPrincipal.dart';
import 'package:tfgapp/pages/Perfil.dart';
import 'package:tfgapp/pages/ReservaPlayers.dart';

import '../clases/Partido.dart';
import '../clases/Pista.dart';
import '../clases/Usuario.dart';

class PartidoDetail extends StatefulWidget{
  static const String ROUTE = "/partido-detail";
  final Partido partido;

  PartidoDetail({required this.partido});

  @override
  _PartidoDetail createState() => _PartidoDetail();
}

class _PartidoDetail extends State<PartidoDetail>{

  late Partido currentPartido;
  late Partido currentPartido2;
  late Partido p;
  List<String> pareja1 = [];
  List<String> pareja2 = [];
  Partido? partidoActual;
  late Future<Usuario> user;

  @override
  void initState(){
    super.initState();
    currentPartido = widget.partido;
    getPista(widget.partido.idPista);
    print('Resultado: ' + widget.partido.resultado);
    print(widget.partido.precioFinal);
    setState(() {

    });
  }

  Future<Usuario> getUser() async{
    var peticiones = Peticiones();
    return await peticiones.getUser();
  }

  Future<Pista> getPista(int id) async{
    var peticiones = Peticiones();
    return await peticiones.getPista(id);
  }

  Future<Club> getClub(int id) async{
    var peticiones = Peticiones();
    return await peticiones.getClub(id);
  }

  Future<void> deleteJugador(Partido partido, Usuario jugador) async {
    var peticiones = Peticiones();
    await peticiones.updatePartidoJugadorDelete(partido, jugador);
  }

  Future<Usuario> getUserByLogin(String login) async{
    var peticiones = Peticiones();
    return await peticiones.getUsuarioByLogin(login);
  }

  Future<Partido> getPartido(int id) async {
    var peticiones = Peticiones();
    p = await peticiones.getPartido(id);
    return await peticiones.getPartido(id);
  }

  Widget _partidoCard(Partido partido) {

    pareja1 = [];
    pareja2 = [];

    for (int i = 0; i < partido.jugadores.length; i++) {
      if (i < 2) {
        pareja1.add(partido.jugadores[i]);
      } else {
        pareja2.add(partido.jugadores[i]);
      }
    }

    if (pareja1.length < 2) {
      pareja1.add('vacio');
    }

    if (pareja2.length < 2) {
      if (pareja2.length == 1)
        pareja2.add('vacio');
      else
        for (int i = 0; i < 2; i++) {
          pareja2.add('vacio');
        }
    }

    return Card(
      margin: EdgeInsets.symmetric(vertical: 10, horizontal: 15),
      child: Padding(
        padding: EdgeInsets.all(17),
        child: LimitedBox(
          maxHeight: 75,
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              if (pareja1.isNotEmpty)
                _generarFilaJugadores(pareja1),
              VerticalDivider(color: Colors.grey, thickness: 0.5, width: 10),
              if (pareja2.isNotEmpty)
                _generarFilaJugadores(pareja2),

            ],
          ),
        ),
      ),
    );

  }

  Widget _generarFilaJugadores(List<String> jugadores) {

    return Row(
      children: [
        ...jugadores.map((jugador) => Container(
          width: 50,
          child: Column(
            children: [
              if (jugador != 'vacio')
                if(DateTime.parse(widget.partido.hora_ini).isAfter(DateTime.now()))
                  Stack(
                  alignment: Alignment.topRight,
                  children: [
                    Icon(Icons.person, size: 40),
                    Positioned(
                      right: 0,
                      child: CircleAvatar(
                        backgroundColor: Colors.white,
                        radius: 12,
                        child: FutureBuilder<Usuario>(
                          future: getUserByLogin(jugador),
                          builder: (context, snapshot){
                            if (snapshot.connectionState == ConnectionState.waiting) {
                              return CircularProgressIndicator();
                            } else if (snapshot.hasError) {
                              return Text("Error: ${snapshot.error}");
                            } else {
                              Usuario user = snapshot.data!;
                              return IconButton(
                                padding: EdgeInsets.zero,
                                icon: Icon(Icons.close, color: Colors.red, size: 20),
                                onPressed: (){
                                  setState(() {
                                    widget.partido.jugadores.remove(jugador);
                                    deleteJugador(widget.partido, user);
                                  });
                                },
                              );
                            }
                          },
                        )
                      ),
                    )
                  ],
                )
                else
                  Icon(Icons.person, size: 40,)
              else
                CircleAvatar(
                  radius: 20,
                  child: IconButton(
                    onPressed: () async{
                      if (DateTime.parse(widget.partido.hora_ini).isAfter(DateTime.now())){
                        final jugadorAnadido = await Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => ReservaPlayers(partido: widget.partido)),
                        );
                        if (jugadorAnadido == true) {
                          setState(() async {
                            currentPartido = await getPartido(widget.partido.id);
                          });
                        }
                      }
                    },
                    icon: Icon(Icons.add, color: Colors.white),
                  ),
                  backgroundColor: Colors.deepPurple,
                ),
              Text(jugador),
            ],
          ),
        )).toList(),
      ],
    );
  }

  Widget _verMarcador(String marcador){

    while(marcador.length < 6){
      marcador += "--";
    }

    List<String> sets = [];
    for (int i = 0; i < marcador.length; i += 2) {
      sets.add(marcador.substring(i, i + 2));
    }

    List<TableRow> filasTabla = [];

    List<Widget> encabezadoSets = [Container()];

    for (int i = 1; i <= 3; i++) {
      encabezadoSets.add(
        Center(child: Text('Set-$i', style: TextStyle(fontSize: 20))),
      );
    }
    filasTabla.add(TableRow(children: encabezadoSets));

    List<Widget> juegosParejaA = [Center( child: Text("Pareja A", style: TextStyle(fontSize: 20)))];
    for (String set in sets) {
      juegosParejaA.add(
        Center(child: Text(set[0], style: TextStyle(fontSize: 20))),
      );
    }
    filasTabla.add(TableRow(children: juegosParejaA));

    List<Widget> juegosParejaB = [Center( child: Text("Pareja B", style: TextStyle(fontSize: 20)))];
    for (String set in sets) {
      juegosParejaB.add(
        Center(child: Text(set[1], style: TextStyle(fontSize: 20))),
      );
    }
    filasTabla.add(TableRow(children: juegosParejaB));

    return Table(
      border: TableBorder.all(width: 1.0, color: Colors.black),
      children: filasTabla,
    );

  }

  String determinarGanador(String resultadoSets) {
    print('Sets: ' + resultadoSets);
    if (resultadoSets.length != 2)
      return "Resultado inválido";

    int setsParejaA = int.parse(resultadoSets[0]);
    int setsParejaB = int.parse(resultadoSets[1]);

    if (setsParejaA == 2) {
      /*if (pareja1.length == 2 && !pareja1.contains('vacio') && pareja2.length ==2 && !pareja2.contains('vacio')) {
        actualizarNivel(pareja1, true);
        actualizarNivel(pareja2, false);
      }*/
      return "La Pareja A es la ganadora";
    } else if (setsParejaB == 2) {
      /*if (pareja1.length == 2 && !pareja1.contains('vacio') && pareja2.length ==2 && !pareja2.contains('vacio')) {
        actualizarNivel(pareja2, true);
        actualizarNivel(pareja1, false);
      }*/
      return "La Pareja B es la ganadora";
    } else {
      return "No se ha determinado un ganador";
    }

  }

  Future<Usuario> getUsuario(int id) async{
    var peticiones = Peticiones();
    return await peticiones.getUsuario(id);
  }

  Future<void> updateNivel(Usuario user, double nivel) async{
    var peticiones = Peticiones();
    await peticiones.updateUserNivel(user, nivel);
  }

  void determinarGanadorNivel(String resultadoSets) async {

    int setsParejaA = int.parse(resultadoSets[0]);
    int setsParejaB = int.parse(resultadoSets[1]);

    if (setsParejaA == 2) {
      if (pareja1.length == 2 && !pareja1.contains('vacio') && pareja2.length ==2 && !pareja2.contains('vacio')) {
        actualizarNivel(pareja1, true);
        actualizarNivel(pareja2, false);
      }
    } else if (setsParejaB == 2) {
      if (pareja1.length == 2 && !pareja1.contains('vacio') && pareja2.length ==2 && !pareja2.contains('vacio')) {
        actualizarNivel(pareja2, true);
        actualizarNivel(pareja1, false);
      }
    }

    setState(() {});
  }

  void actualizarNivel(List<String> jugadores, bool ganaron) async{
    var peticiones = Peticiones();
    for(String login in jugadores){
      Usuario jugador = await peticiones.getUsuarioByLogin(login);

      print("Antes: ${jugador.login} ${jugador.nivel}");

      if (ganaron){
        jugador.nivel += 0.12;
      } else {
        jugador.nivel -= 0.12;
      }
      print("Despues: ${jugador.login} ${jugador.nivel}");
      await updateNivel(jugador, jugador.nivel);
    }
  }

  @override
  Widget build (BuildContext context){
    return WillPopScope(
      onWillPop: () async {
        Navigator.pop(context, widget.partido);
        return false;
      },
      child: Scaffold(
        appBar: AppBar(
          title: Text('Detalles partido'),
          backgroundColor: Colors.deepPurple,
        ),
        body: Padding(
          padding: const EdgeInsets.all(15.0),
            child: ListView(
              children: <Widget>[
                Card(
                  elevation: 4.0,
                  child: Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text('Informacion:',
                          style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)
                        ),
                        SizedBox(height: 10),
                        Row(
                          children: [
                            Icon(Icons.date_range),
                            SizedBox(width: 10),
                            Text('Fecha: ${DateFormat('dd/MM/yyyy').format(DateTime.parse(widget.partido.hora_ini))}',
                                style: TextStyle(fontSize: 15)),
                          ],
                        ),
                        SizedBox(height: 10),
                        Row(
                          children: [
                            Icon(Icons.timer),
                            SizedBox(width: 10),
                            Text(
                                'Hora: ${DateFormat('HH:mm').format(DateTime.parse(widget.partido.hora_ini))} - ${DateFormat('HH:mm').format(DateTime.parse(widget.partido.hora_fin))}',
                                style: TextStyle(fontSize: 15)),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
                SizedBox(height: 20),
                FutureBuilder<Pista>(
                    future: getPista(widget.partido.idPista),
                    builder: (context, snapshot) {
                      if (snapshot.connectionState == ConnectionState.done) {
                        if (snapshot.hasError) {
                          return ListTile(
                            leading: Icon(Icons.error),
                            title: Text('Error al cargar el creador'),
                          );
                        } else if (snapshot.hasData) {
                          Pista pista = snapshot.data!;
                          return FutureBuilder<Club>(
                            future: getClub(pista.club),
                            builder: (context, clubSnapshot) {
                              if (clubSnapshot.connectionState ==
                                  ConnectionState.done) {
                                if (clubSnapshot.hasError) {
                                  return ListTile(
                                    leading: Icon(Icons.error),
                                    title: Text('Error al cargar el creador'),
                                  );
                                } else if (clubSnapshot.hasData) {
                                  Club club = clubSnapshot.data!;
                                  return Card(
                                    elevation: 4.0,
                                    child: Padding(
                                      padding: const EdgeInsets.all(15.0),
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Text('Instalacion:',
                                              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)
                                          ),
                                          SizedBox(height: 10),
                                          Row(
                                            children: [
                                              Icon(Icons.place),
                                              SizedBox(width: 10),
                                              Text('Club: ${club.nombre}',
                                                  style: TextStyle(fontSize: 15)
                                              ),
                                            ],
                                          ),
                                          SizedBox(height: 10),
                                          Row(
                                            children: [
                                              Icon(Icons.sports_tennis),
                                              SizedBox(width: 10),
                                              Text('Pista: ${pista.nombre}',
                                                  style: TextStyle(fontSize: 15)
                                              ),
                                            ],
                                          ),
                                          SizedBox(height: 10),
                                          Row(
                                            children: [
                                              Icon(Icons.monetization_on),
                                              SizedBox(width: 10),
                                              Text('Precio: ${widget.partido.precioFinal}€',
                                                  style: TextStyle(fontSize: 15)
                                              ),
                                            ],
                                          ),
                                          SizedBox(height: 10),
                                          if (widget.partido.pagado)
                                            Row(
                                              children: [
                                                Icon(Icons.check),
                                                SizedBox(width: 10),
                                                Text('Pagado por tarjeta',
                                                  style: TextStyle(fontSize: 15)
                                                )
                                              ]
                                            )
                                          else
                                            Row(
                                            children: [
                                              Icon(Icons.close),
                                              SizedBox(width: 10),
                                              Text('Pagar en club',
                                                style: TextStyle(fontSize: 15)
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ),
                                  );
                                } else {
                                  return ListTile(
                                    leading: Icon(Icons.error),
                                    title: Text('No se encontró club'),
                                  );
                                }
                              }
                              else {
                                return Center(child: CircularProgressIndicator());
                              }
                            },
                          );
                        } else {
                          return ListTile(
                            leading: Icon(Icons.error),
                            title: Text('No se encontró usuario'),
                          );
                        }
                      } else {
                        return Center(child: CircularProgressIndicator());
                      }
                    }
                ),
                SizedBox(height: 20),
                Card(
                  elevation: 4.0,
                  child: Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text('Jugadores:',
                            style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)
                        ),
                        SizedBox(height: 10),
                        Row(
                          children: [
                            SizedBox(width: 60),
                            Text('Pareja A', style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
                            SizedBox(width: 60),
                            Text('Pareja B', style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
                          ],
                        ),
                        Row(
                          children: [
                            SizedBox(width: 15),
                            _partidoCard(widget.partido),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
                SizedBox(height: 20),
                if( DateTime.parse(widget.partido.hora_fin).isBefore(DateTime.now()))
                  if (widget.partido.resultado != "")
                    Card(
                      elevation: 4.0,
                      child: Padding(
                        padding: const EdgeInsets.all(15.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text('Resultado:',
                                style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)
                            ),
                            SizedBox(height: 10),
                            _verMarcador(widget.partido.resultado),
                            SizedBox(height: 10),
                            Row(
                              children: [
                                Text(determinarGanador(widget.partido.resultadoSets),
                                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)
                                ),
                                SizedBox(width: 10),
                                Icon(FontAwesomeIcons.trophy, color: Colors.yellowAccent,),
                              ],
                            ),
                          ],
                        ),
                      ),
                    )
                  else
                    Card(
                    elevation: 4.0,
                    child: Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text('Resultado:',
                              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)
                          ),
                          SizedBox(height: 10),
                          Row(
                            children: [
                              SizedBox(width: 70),
                              ElevatedButton(
                                  style: ElevatedButton.styleFrom(
                                    backgroundColor: Colors.deepPurple, // Color de fondo
                                    foregroundColor: Colors.white, // Color del texto
                                    padding: EdgeInsets.symmetric(horizontal: 30, vertical: 15),
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(10.0),
                                    ),
                                  ),
                                  onPressed: () async{
                                    final resultAnadido = await Navigator.push(
                                        context,
                                        MaterialPageRoute(builder: (context) => AddResultado(partido: widget.partido)),
                                    );
                                    if (resultAnadido == true) {

                                      currentPartido2 = await getPartido(widget.partido.id);
                                      setState((){
                                        determinarGanadorNivel(widget.partido.resultadoSets);
                                      });

                                      var usuarioActualizado = await getUser(); // Asumiendo que este es el método que te da la info del usuario
                                      setState(() {
                                        user = Future.value(usuarioActualizado);
                                      });
                                      Navigator.pushNamed(context, Perfil.ROUTE);
                                    }
                                  },
                                  child: Text('Añadir Resultado')
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
              ],
            ),
        ),
      ),
    );
  }
}