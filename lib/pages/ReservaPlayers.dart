
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tfgapp/api/peticiones.dart';
import 'package:tfgapp/pages/PartidoDetail.dart';

import '../clases/Partido.dart';
import '../clases/Usuario.dart';

class ReservaPlayers extends StatefulWidget{

  static const String ROUTE = "/reserva-players";
  final Partido partido;

  ReservaPlayers({required this.partido});

  @override
  _ReservaPlayers createState() => _ReservaPlayers();
}

class _ReservaPlayers extends State<ReservaPlayers>{

  final TextEditingController textEditingController = TextEditingController();
  List<Usuario> jugadores = [];
  late Future<List<Usuario>> listaUsuarios;
  List<Usuario> jugadoresFiltrados = [];
  List<String> jugadoresPartido = [];

  @override
  void initState() {
    super.initState();
    jugadoresPartido = widget.partido.jugadores;
    getAllUsers().then((value){
      jugadores = value.where((jugador) => !jugadoresPartido.contains(jugador.login)).toList();
      jugadoresFiltrados = jugadores;
      setState(() {});
    });

  }

  Future<List<Usuario>> getAllUsers() async {
    var peticiones = Peticiones();
    listaUsuarios = peticiones.jugadores();
    return await peticiones.jugadores();
  }

  Future<void> addJugador(Partido partido, Usuario jugador) async{
    var peticiones = Peticiones();
    await peticiones.updatePartidoJugador(partido, jugador);

    Navigator.pop(context, true);
  }

  Widget _usuarioCard(Usuario jugador){
    return Card(
      child: Center(
        child: Column(
          children: [
            SizedBox(height: 10),
            CircleAvatar(
              radius: 20,
              child: Icon(Icons.person, color: Colors.black),
                backgroundColor: Colors.grey,
            ),
            SizedBox(height: 10),
            Text(jugador.login, style: TextStyle(fontSize: 15),),
            SizedBox(height: 10),
            Text(jugador.nivel.toString()),
            SizedBox(height: 10),
            ElevatedButton(
                style: ElevatedButton.styleFrom(
                  backgroundColor: Colors.deepPurple, // Color de fondo
                  foregroundColor: Colors.white, // Color del texto
                  padding: EdgeInsets.symmetric(horizontal: 30, vertical: 15),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                ),
              onPressed: () async{
                await addJugador(widget.partido, jugador);
                Navigator.pop(context, widget.partido);
              },
              child: Text('Añadir')
            ),
            SizedBox(height: 10),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context){

    return Scaffold(
      appBar: AppBar(
        title: Text('Añadir Jugador'),
        backgroundColor: Colors.deepPurple,
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(15),
            child: TextField(
              controller: textEditingController,
              decoration: InputDecoration(
                labelText: "Buscar jugador",
                prefixIcon: Icon(Icons.search),
                border: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(25.0))),
              ),
              onChanged: (value) {
                setState(() {
                  jugadoresFiltrados = jugadores.where((jugador) => jugador.login.toLowerCase().contains(value.toLowerCase())
                                                && !widget.partido.jugadores.contains(jugador.login)).toList();
                });
              },
            ),
          ),
          Text('Usuarios:', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
          Expanded(
            child: FutureBuilder<List<Usuario>>(
              future: listaUsuarios,
              builder: (context, snapshot){
                if (snapshot.connectionState == ConnectionState.waiting) {
                  // Muestra un indicador de carga mientras esperamos los datos
                  return Center(child: CircularProgressIndicator());
                } else if (snapshot.hasError) {
                  // Muestra algún mensaje de error si hay un error
                  return Center(child: Text('Error al cargar los usuarios'));
                } else {
                  jugadores = snapshot.data!;
                  return GridView.builder(
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2,
                      crossAxisSpacing: 10.0,
                      mainAxisSpacing: 10.0,
                    ),
                    itemCount: jugadoresFiltrados.length,
                    itemBuilder: (context, index) {
                      return _usuarioCard(jugadoresFiltrados[index]);
                    },
                  );
                }
              },
            ),

          ),
        ],
      ),
    );
  }
}