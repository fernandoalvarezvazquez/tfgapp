import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tfgapp/api/peticiones.dart';
import 'package:tfgapp/clases/Usuario.dart';

import 'FormRegister.dart';

class RegisterPage extends StatefulWidget{
  static const String ROUTE = "/register";

  @override
  _RegisterPage createState() => _RegisterPage();
}

class _RegisterPage extends State<RegisterPage>{
  final TextEditingController _loginController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.deepPurple,
        title: Text('Registro'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            TextField(
              controller: _loginController,
              decoration: InputDecoration(labelText: 'Login'),
            ),
            SizedBox(height: 16),
            TextField(
              controller: _passwordController,
              decoration: InputDecoration(labelText: 'Contraseña'),
              obscureText: true,
            ),
            SizedBox(height: 32),
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                backgroundColor: Colors.deepPurple, // Color de fondo
                foregroundColor: Colors.white, // Color del texto
                padding: EdgeInsets.symmetric(horizontal: 30, vertical: 15),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
              ),
              onPressed: _registrarUsuario,
              child: Text('Registrarse'),
            )
          ],
        ),
      ),
    );
  }

  Future<bool> loginExiste(String login) async{
    List<Usuario> users = await Peticiones().usuarios();
    if (users.any((user) => user.login == login)){
      return true;
    }
    else return false;
  }

  void _registrarUsuario() async{
    final login = _loginController.text;
    final password = _passwordController.text;
    bool existeLogin = await loginExiste(login);

    if(existeLogin){
      ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text('El login ya existe. Por favor, elige otro.'),
            backgroundColor: Colors.red,
          )
      );
    }
    else if (login.isNotEmpty && password.isNotEmpty) {
      if (login.length >= 4){
        print("Registrando usuario con login: $login y contraseña: $password");
        await Peticiones().registerUser(login, password);
        await Peticiones().autenticar(login, password);
        Navigator.pushNamed(context, FormRegister.ROUTE);
      } else {
        ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(content: Text('El login debe tener minimo 4 caracteres'), backgroundColor: Colors.red,)
        );
      }
    } else {
      ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(content: Text('Por favor, rellena todos los campos.'), backgroundColor: Colors.red,)
      );
    }
  }
}