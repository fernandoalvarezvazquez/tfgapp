
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tfgapp/api/peticiones.dart';
import 'package:tfgapp/pages/PartidoDetail.dart';

import '../clases/Partido.dart';

class AddResultado extends StatefulWidget{
  static const String ROUTE = "/add-result";
  final Partido partido;

  AddResultado({required this.partido});

  @override
  _AddResultado createState() => _AddResultado();
}

class _AddResultado extends State<AddResultado>{
  String? resultado;
  final _formKey = GlobalKey<FormState>();

  TextEditingController set1ParejaAController = TextEditingController();
  TextEditingController set1ParejaBController = TextEditingController();
  TextEditingController set2ParejaAController = TextEditingController();
  TextEditingController set2ParejaBController = TextEditingController();
  TextEditingController set3ParejaAController = TextEditingController();
  TextEditingController set3ParejaBController = TextEditingController();

  int setsA = 0;
  int setsB = 0;

  Future<void> addResult(int id, Partido partido) async{
    var peticiones = Peticiones();
    await peticiones.updateResultado(id, partido);
    Navigator.pop(context, true);
  }

  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        title: Text('Añadir Resultado'),
        backgroundColor: Colors.deepPurple,
      ),
      body: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Center(child: Text('Añada el resultado', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold))),
              SizedBox(height: 20,),
              Table(
                border: TableBorder.all(),
                children: [
                  TableRow(
                    children: [
                      TableCell(
                        child: SizedBox(
                          height: 50.0,
                          child: Center(child: Text(''))
                        )
                      ),
                      TableCell(
                        verticalAlignment: TableCellVerticalAlignment.middle,
                        child: Center(child: Text('Set 1'))
                      ),
                      TableCell(
                          verticalAlignment: TableCellVerticalAlignment.middle,
                          child: Center(child: Text('Set 2'))
                      ),
                      TableCell(
                          verticalAlignment: TableCellVerticalAlignment.middle,
                          child: Center(child: Text('Set 3'))
                      ),
                    ],
                  ),
                  TableRow(
                    children: [
                      TableCell(
                        verticalAlignment: TableCellVerticalAlignment.middle,
                        child: Center(child: Text('Pareja A'))
                      ),
                      TableCell(
                        verticalAlignment: TableCellVerticalAlignment.middle,
                        child: TextFormField(
                          controller: set1ParejaAController,
                          keyboardType: TextInputType.number,
                          textAlign: TextAlign.center,
                          decoration: InputDecoration(
                            hintText: set1ParejaAController.text.isEmpty ? "-" : null,
                            border: InputBorder.none,
                          ),
                          validator: (value) {
                            if (value == null ||
                                value.isEmpty ||
                                int.tryParse(value) == null ||
                                int.parse(value) < 0 ||
                                int.parse(value) > 7) {
                              return '0-7';
                            }
                            return null;
                          },
                        ),
                      ),
                      TableCell(
                        verticalAlignment: TableCellVerticalAlignment.middle,
                        child: TextFormField(
                          controller: set2ParejaAController,
                          keyboardType: TextInputType.number,
                          textAlign: TextAlign.center,
                          decoration: InputDecoration(
                            hintText: set2ParejaAController.text.isEmpty ? "-" : null,
                            border: InputBorder.none,
                          ),
                          validator: (value) {
                            if (value == null ||
                                value.isEmpty ||
                                int.tryParse(value) == null ||
                                int.parse(value) < 0 ||
                                int.parse(value) > 7) {
                              return '0-7';
                            }
                            return null;
                          },
                        ),
                      ),
                      TableCell(
                        verticalAlignment: TableCellVerticalAlignment.middle,
                        child: TextFormField(
                          controller: set3ParejaAController,
                          keyboardType: TextInputType.number,
                          textAlign: TextAlign.center,
                          decoration: InputDecoration(
                            hintText: set3ParejaAController.text.isEmpty ? "-" : null,
                            border: InputBorder.none,
                          ),
                          validator: (value) {
                            if (value != null &&
                                value.isNotEmpty &&
                                (int.tryParse(value) == null ||
                                    int.parse(value) < 0 ||
                                    int.parse(value) > 7)) {
                              return '0-7';
                            }
                            return null;
                          },
                        ),
                      ),
                    ]
                  ),
                  TableRow(
                    children: [
                      TableCell(
                        verticalAlignment: TableCellVerticalAlignment.middle,
                        child: Center(child: Text('Pareja B'))
                      ),
                      TableCell(
                        verticalAlignment: TableCellVerticalAlignment.middle,
                        child: TextFormField(
                          controller: set1ParejaBController,
                          keyboardType: TextInputType.number,
                          textAlign: TextAlign.center,
                          decoration: InputDecoration(
                            hintText: set1ParejaBController.text.isEmpty ? "-" : null,
                            border: InputBorder.none,
                          ),
                          validator: (value) {
                            if (value == null ||
                                value.isEmpty ||
                                int.tryParse(value) == null ||
                                int.parse(value) < 0 ||
                                int.parse(value) > 7) {
                              return '0-7';
                            }
                            return null;
                          },
                        ),
                      ),
                      TableCell(
                        verticalAlignment: TableCellVerticalAlignment.middle,
                        child: TextFormField(
                          controller: set2ParejaBController,
                          keyboardType: TextInputType.number,
                          textAlign: TextAlign.center,
                          decoration: InputDecoration(
                            hintText: set2ParejaBController.text.isEmpty ? "-" : null,
                            border: InputBorder.none,
                          ),
                          validator: (value) {
                            if (value == null ||
                                value.isEmpty ||
                                int.tryParse(value) == null ||
                                int.parse(value) < 0 ||
                                int.parse(value) > 7) {
                              return '0-7';
                            }
                            return null;
                          },
                        ),
                      ),
                      TableCell(
                        verticalAlignment: TableCellVerticalAlignment.middle,
                        child: TextFormField(
                          controller: set3ParejaBController,
                          keyboardType: TextInputType.number,
                          textAlign: TextAlign.center,
                          decoration: InputDecoration(
                            hintText: set3ParejaBController.text.isEmpty ? "-" : null,
                            border: InputBorder.none,
                          ),
                          validator: (value) {
                            if (value != null &&
                                value.isNotEmpty &&
                                (int.tryParse(value) == null ||
                                    int.parse(value) < 0 ||
                                    int.parse(value) > 7)) {
                              return '0-7';
                            }
                            return null;
                          },
                        ),
                      ),
                    ],
                  ),
                ],
              ),
              SizedBox(height: 20),
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                  backgroundColor: Colors.deepPurple, // Color de fondo
                  foregroundColor: Colors.white, // Color del texto
                  padding: EdgeInsets.symmetric(horizontal: 30, vertical: 15),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                ),
                onPressed: () async {
                  if (_formKey.currentState!.validate()) {
                    resultado = "${set1ParejaAController.text}${set1ParejaBController.text}"
                        "${set2ParejaAController.text}${set2ParejaBController.text}"
                        "${set3ParejaAController.text}${set3ParejaBController.text}";
                    print(resultado);

                    for (int i = 0; i<resultado!.length;i+=2){
                      if(resultado?[i] == '7') {
                        setsA += 1;
                      } else if(resultado?[i+1] =='7') {
                        setsB += 1;
                      } else if(resultado?[i] == '6') {
                        setsA += 1;
                      } else if(resultado?[i+1] == '6') {
                        setsB += 1;
                      }
                    }

                    widget.partido.resultado = resultado!;
                    widget.partido.resultadoSets = setsA.toString() + setsB.toString();
                    await addResult(widget.partido.id, widget.partido);
                    Navigator.pop(context, widget.partido);
                  }
                },
                child: Text('Guardar Resultado'),
              ),
            ],
          ),
        ),
      ),
    );
  }

}