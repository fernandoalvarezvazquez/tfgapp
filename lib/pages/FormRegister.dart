import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tfgapp/pages/MenuPrincipal.dart';

import '../api/peticiones.dart';
import '../clases/Usuario.dart';

class FormRegister extends StatefulWidget{
  static const String ROUTE = "/register-form";

  @override
  _FormRegister createState() => _FormRegister();
}

class _FormRegister extends State<FormRegister>{
  final _formKey = GlobalKey<FormState>();
  String? raquetaRespuesta;
  String? anosRespuesta;
  String? padelRespuesta;
  String? vecesPorSemanaRespuesta;
  double nivel = 0;
  late Future<Usuario> user;

  @override
  void initState() {
    super.initState();
    getCurrentUser();
  }

  Future<Usuario> getCurrentUser() async{
    var peticiones = Peticiones();
    user = peticiones.getUser();
    return await user;
  }

  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.deepPurple,
        title: Text('Formulario registro'),
      ),
      body: Form(
        key: _formKey,
        child: ListView(
          padding: EdgeInsets.all(16),
          children: [
          DropdownButtonFormField<String>(
            hint: Text('¿Has jugado alguna vez a deportes de raqueta?'),
            value: raquetaRespuesta,
            items: ['Sí', 'No'].map((String value) {
            return DropdownMenuItem<String>(
              value: value,
              child: Text(value),
            );
            }).toList(),
            onChanged: (newValue) {
              setState(() {
                raquetaRespuesta = newValue;
              });
            },
            validator: (value) {
              if (value == null || value.isEmpty) {
                return 'Por favor selecciona una opción';
              }
              return null;
            },
          ),
          SizedBox(height: 20),
          TextFormField(
            keyboardType: TextInputType.number,
            decoration: InputDecoration(
              labelText: '¿Durante cuántos años?',
            ),
            onSaved: (value) {
            anosRespuesta = value;
            },
            validator: (value) {
              if (value == null || value.isEmpty) {
                return 'Por favor ingresa un número';
              }
              return null;
            },
          ),
          SizedBox(height: 20),
          DropdownButtonFormField<String>(
            hint: Text('¿Has jugado al padel con anterioridad?'),
            value: padelRespuesta,
            items: ['Sí', 'No'].map((String value) {
              return DropdownMenuItem<String>(
                value: value,
                child: Text(value),
              );
            }).toList(),
            onChanged: (newValue) {
              setState(() {
                padelRespuesta = newValue;
              });
            },
            validator: (value) {
              if (value == null || value.isEmpty) {
                return 'Por favor selecciona una opción';
              }
              return null;
            },
          ),
          SizedBox(height: 20),
          TextFormField(
            keyboardType: TextInputType.number,
            decoration: InputDecoration(
              labelText: '¿Cuántas veces por semana de media juegas?',
            ),
            onSaved: (value) {
              vecesPorSemanaRespuesta = value;
            },
            validator: (value) {
              if (value == null || value.isEmpty) {
                return 'Por favor ingresa un número';
              }
              return null;
            },
          ),
          SizedBox(height: 20),
          ElevatedButton(
            style: ElevatedButton.styleFrom(
              backgroundColor: Colors.deepPurple, // Color de fondo
              foregroundColor: Colors.white, // Color del texto
              padding: EdgeInsets.symmetric(horizontal: 30, vertical: 15),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0),
              ),
            ),
            onPressed: _submitForm,
            child: Text('Enviar respuestas'),
          ),
          ],
        ),
      ),
    );
  }

  void _submitForm() async{

    if (_formKey.currentState!.validate()) {
      _formKey.currentState!.save();

      if(raquetaRespuesta == "Sí")
        nivel += 0.50;
      if(anosRespuesta != null)
        if(int.parse(anosRespuesta!) > 0)
          nivel += (0.2*int.parse(anosRespuesta!));
      if (padelRespuesta == "Sí")
        nivel += 1.0;
      if (vecesPorSemanaRespuesta != null)
        if (int.parse(vecesPorSemanaRespuesta!) > 0)
          nivel += (int.parse(vecesPorSemanaRespuesta!)*0.25);
      if (nivel <= 1.0)
        nivel = 1.0;

      Usuario user = await Peticiones().getUser();
      user.nivel = nivel;
      user.fotoPerfil = "";
      await Peticiones().updateUser(user.id, user);

      print('Respuestas:');
      print('¿Has jugado alguna vez a deportes de raqueta? $raquetaRespuesta');
      print('¿Durante cuántos años? $anosRespuesta');
      print('¿Has jugado al padel con anterioridad? $padelRespuesta');
      print('¿Cuántas veces por semana de media? $vecesPorSemanaRespuesta');
      print('Tu nivel seria: $nivel');

      Navigator.pushNamed(context, MenuPrincipal.ROUTE);
    }
  }
}