import 'package:flutter/material.dart';

import 'AuntenticacionPage.dart';
import 'RegisterPage.dart';

class Inicio extends StatelessWidget{
  static const String  ROUTE= "/";
  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.deepPurple,
        title: Text('Inicio app TFG'),
      ),
      body: Center(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Padding(padding: EdgeInsets.all(16.0),
                child: Text(
                  '¡Bienvenido a mi aplicacion movil! Por favor, si aun no esta registrado, registrese para acceder a la aplicacion',
                  style: TextStyle(fontSize: 20.0),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                  backgroundColor: Colors.deepPurple, // Color de fondo
                  foregroundColor: Colors.white, // Color del texto
                  padding: EdgeInsets.symmetric(horizontal: 30, vertical: 15),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                ),
                onPressed: () {
                  Navigator.pushNamed(
                    context,
                    RegisterPage.ROUTE,
                  );
                },
                child: Text('Registrarme'),
              ),
              SizedBox(
                height: 20,
              ),
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                  backgroundColor: Colors.deepPurple, // Color de fondo
                  foregroundColor: Colors.white, // Color del texto
                  padding: EdgeInsets.symmetric(horizontal: 30, vertical: 15),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                ),
                onPressed: () {
                  Navigator.pushNamed(
                    context,
                    AutenticacionPage.ROUTE,
                  );
                },
                child: Text('Autenticarme'),
              ),
            ],
          ),
        ),
      ),
    );
  }
}