import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:tfgapp/clases/Partido.dart';
import 'package:tfgapp/pages/BonosList.dart';
import 'package:tfgapp/pages/MenuLateral.dart';
import '../api/peticiones.dart';
import '../clases/Usuario.dart';
import 'EditarPerfil.dart';
import 'MenuPrincipal.dart';
import 'PartidoDetail.dart';

class Perfil extends StatefulWidget {
  static const String ROUTE = "/perfil";

  @override
  _Perfil createState() => _Perfil();
}

class _Perfil extends State<Perfil> {
  late Future<Usuario> user;
  late Future<List<Partido>> partidos;
  int _selectedIndex = 2;

  final picker = ImagePicker();
  File? _selectedImage;

  @override
  void initState() {
    super.initState();
    getCurrentUser();
    setState(() {

    });
  }

  Future<Usuario> getCurrentUser() async {
    var peticiones = Peticiones();
    user = peticiones.getUser();
    return await user;
  }
  
  Future<Usuario> getUserByLogin(String login) async{
    var peticiones = Peticiones();
    return await peticiones.getUsuarioByLogin(login);
  }

  Future<List<Partido>> getPartidosJugados(id) async {
    var peticiones = Peticiones();
    partidos = peticiones.getPartidosJugadosUser(id);
    return await partidos;
  }

  Widget _partidoCard(Partido partido) {
    DateTime fecha = DateTime.parse(partido.hora_ini);

    // Separar jugadores en dos listas
    List<String> pareja1 = [];
    List<String> pareja2 = [];

    for (int i = 0; i < partido.jugadores.length; i++) {
      if (i < 2) {
        pareja1.add(partido.jugadores[i]);
      } else {
        pareja2.add(partido.jugadores[i]);
      }
    }

    if (pareja1.length < 2){
      pareja1.add('vacio');
    }

    if(pareja2.length < 2){
      if (pareja2.length == 1)
        pareja2.add('vacio');
      else
        for(int i = 0; i<2; i++){
          pareja2.add('vacio');
        }
    }

    return Card(
      margin: EdgeInsets.symmetric(vertical: 10, horizontal: 15),
      child: Padding(
        padding: EdgeInsets.all(10),
        child: LimitedBox(
          maxHeight: 750,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text(
                "${fecha.day}/${fecha.month}/${fecha.year}",
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
              ),
              SizedBox(height: 10),
              if (pareja1.isNotEmpty)
                _generarFilaJugadores(pareja1),
              Divider(color: Colors.black,  // Asegúrate de que el color sea visible contra el fondo
                thickness: 2.0,       // Puedes ajustar el espesor si es necesario
                height: 20.0
              ),
              if (pareja2.isNotEmpty)
                _generarFilaJugadores(pareja2),
              TextButton(
                  onPressed: () async{
                    final updatedPartido = await Navigator.push<Partido?>(
                      context,
                      MaterialPageRoute(
                        builder: (context) => PartidoDetail(partido: partido),
                      ),
                    );

                    if (updatedPartido != null) {
                      setState(() {
                        if(partido.id == updatedPartido.id){
                          partido = updatedPartido;
                        }
                      });
                    }
                  },
                style: TextButton.styleFrom(
                  backgroundColor: Colors.deepPurple,
                  foregroundColor: Colors.white,
                  padding: EdgeInsets.symmetric(horizontal: 15, vertical: 5),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5.0),
                  ),

                ),
                child: Text('Ver Detalles')
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _generarFilaJugadores(List<String> jugadores) {

    return Row(
      children: [
        ...jugadores.map((jugador) => Container(
          width: 50,
          child: Column(
            children: [
              if (jugador != 'vacio')
                FutureBuilder<Usuario>(
                  future: getUserByLogin(jugador),
                  builder: (context, snapshot) {
                    if (snapshot.connectionState == ConnectionState.done) {
                      if (snapshot.hasError) {
                        return ListTile(
                          leading: Icon(Icons.error),
                          title: Text('Error al cargar el creador'),
                        );
                      } else if (snapshot.hasData) {
                        Usuario usuario = snapshot.data!;
                        if (usuario.fotoPerfil.isEmpty)
                          return Icon(Icons.person, color: Colors.black, size: 40.0);
                        else return CircleAvatar(
                          radius: 20,
                          backgroundImage: NetworkImage('http://10.0.2.2:8080/api/usuarios/${usuario.id}/foto-perfil'),
                        );
                      } else {
                        return ListTile(
                          leading: Icon(Icons.error),
                          title: Text('No se encontró usuario'),
                        );
                      }
                    } else {
                      return Center(child: CircularProgressIndicator());
                    }
                  },
                )
              else
                CircleAvatar(
                  radius: 20,
                  child: Icon(Icons.add, color: Colors.white),
                  backgroundColor: Colors.deepPurple,
                ),
              Text(jugador),
            ],
          ),
        )).toList(),
      ],
    );
  }

  Future<void> _selectImage(Usuario user) async {
    final pickedFile = await picker.pickImage(source: ImageSource.gallery);

    setState(() {
      if (pickedFile != null) {
        _selectedImage = File(pickedFile.path);
        Peticiones().updateFotoUser(user, _selectedImage!.path);
      } else {
        print('No se seleccionó ninguna imagen.');
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      endDrawer: MenuLateral(),
      appBar: AppBar(
        backgroundColor: Colors.deepPurple,
        title: Center(
          child: Text('Perfil de usuario'),
        ),
        leading: Container(),
      ),
      body: Container(
        child: Column(
          children: [
            SizedBox(height: 20),
            Center(
              child: Container(
                width: 100,
                height: 100,
                decoration: BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black26,
                      blurRadius: 6,
                      offset: Offset(0, 2),
                    ),
                  ],
                  shape: BoxShape.circle,
                ),
                child:FutureBuilder<Usuario>(
                    future: getCurrentUser(),
                    builder: (context, snapshot) {
                      if (snapshot.connectionState == ConnectionState.done) {
                        if (snapshot.hasError) {
                        return ListTile(
                        leading: Icon(Icons.error),
                        title: Text('Error al cargar el creador'),
                        );
                        } else if (snapshot.hasData) {
                          Usuario usuario = snapshot.data!;
                          print(usuario.nivel);
                          if (usuario.fotoPerfil.isEmpty)
                            return Stack(
                            children: [
                              CircleAvatar(
                                radius: 50,
                                backgroundColor: Colors.deepPurple[400],
                                child: Icon(Icons.person, color: Colors.white, size: 70.0),
                              ),
                              Positioned(
                                bottom: 0,
                                right: 0,
                                child: Container(
                                  decoration: BoxDecoration(
                                    color: Colors.white,
                                    shape: BoxShape.circle,
                                    boxShadow: [
                                      BoxShadow(
                                        color: Colors.black26,
                                        blurRadius: 6,
                                        offset: Offset(0, 2),
                                      ),
                                    ],
                                  ),
                                  child: IconButton(
                                      icon: Icon(Icons.camera_alt, color: Colors.deepPurple[400]),
                                      onPressed: (){
                                      _selectImage(usuario);
                                      }
                                  ),
                                ),
                              ),
                            ],
                          );
                          else return Stack(
                            children: [
                              CircleAvatar(
                                radius: 50,
                                backgroundImage: NetworkImage('http://10.0.2.2:8080/api/usuarios/${usuario.id}/foto-perfil'),
                              ),
                              Positioned(
                                bottom: 0,
                                right: 0,
                                child: Container(
                                  decoration: BoxDecoration(
                                    color: Colors.white,
                                    shape: BoxShape.circle,
                                    boxShadow: [
                                      BoxShadow(
                                        color: Colors.black26,
                                        blurRadius: 6,
                                        offset: Offset(0, 2),
                                      ),
                                    ],
                                  ),
                                  child: IconButton(
                                    icon: Icon(Icons.camera_alt, color: Colors.deepPurple[400]),
                                    onPressed: () {
                                      _selectImage(usuario);
                                    },
                                  ),
                                ),
                              ),
                            ],
                          );
                        } else {
                          return ListTile(
                            leading: Icon(Icons.error),
                            title: Text('No se encontró usuario'),
                          );
                        }
                      } else {
                        return Center(child: CircularProgressIndicator());
                      }
                    },
                ),
              ),
            ),
            SizedBox(height: 20),
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                backgroundColor: Colors.white, // Color de fondo
                foregroundColor: Colors.black, // Color del texto
                padding: EdgeInsets.symmetric(horizontal: 30, vertical: 15),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
              ),
              onPressed: () {
                Navigator.pushNamed(
                  context,
                  EditarPerfil.ROUTE,
                );
              },
              child: Text('Editar perfil'),
            ),
            SizedBox(height: 20),
            Expanded(
              child: FutureBuilder<Usuario>(
                future: getCurrentUser(),
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.done) {
                    if (snapshot.hasError) {
                      return ListTile(
                        leading: Icon(Icons.error),
                        title: Text('Error al cargar el creador'),
                      );
                    } else if (snapshot.hasData) {
                      Usuario usuario = snapshot.data!;
                      print(usuario.nivel);
                      return FutureBuilder<List<Partido>>(
                          future: getPartidosJugados(usuario.id),
                          builder: (context, partidoSnapshot) {
                            if (partidoSnapshot.connectionState ==
                                ConnectionState.done) {
                              if (partidoSnapshot.hasError) {
                                print(partidoSnapshot.error);
                                return ListTile(
                                  leading: Icon(Icons.error),
                                  title: Text('Error al cargar los partidos'),
                                );
                              } else if (partidoSnapshot.hasData) {
                                List<Partido> partidos = partidoSnapshot.data!;
                                return SingleChildScrollView(
                                  child: Card(
                                    margin: EdgeInsets.symmetric(
                                        horizontal: 20, vertical: 10),
                                    child: Column(
                                      children: [
                                        ListTile(
                                          leading: Icon(
                                            Icons.person,
                                            size: 50,
                                          ),
                                          title: Text('Nombre de usuario'),
                                          subtitle: Text(
                                            '${usuario.login}',
                                            style: TextStyle(fontSize: 24.0),
                                          ),
                                        ),
                                        ListTile(
                                          leading: Icon(
                                            Icons.graphic_eq,
                                            size: 50,
                                          ),
                                          title: Text('Nivel'),
                                          subtitle: Text(
                                            '${double.parse(usuario.nivel.toStringAsFixed(2))}',
                                            style: TextStyle(fontSize: 24.0),
                                          ),
                                        ),
                                        Container(
                                          width: MediaQuery.of(context).size.width * 0.7, // 70% del ancho total
                                          child: Divider(
                                            color: Colors.grey,
                                          ),
                                        ),
                                        Text(
                                          'Mis partidos jugados:',
                                          style: TextStyle(fontSize: 20),
                                        ),
                                        if (partidos.isNotEmpty)
                                        Container(
                                          height: 250,
                                          child: ListView.builder(
                                            scrollDirection: Axis.horizontal,
                                            itemCount: partidos?.length ?? 0,
                                            itemBuilder: (context, index) {
                                              return _partidoCard(
                                                  partidos[index]);
                                            },
                                          ),
                                        )
                                        else 
                                          Container(
                                            height: 250,
                                            child: Center(
                                              child: Text('Aun no se han jugado partidos'),
                                            ),
                                          )
                                      ],
                                    ),
                                  ),
                                );
                              } else {
                                return ListTile(
                                  leading: Icon(Icons.error),
                                  title: Text('No se encontraron partidos'),
                                );
                              }
                            } else {
                              return Center(child: CircularProgressIndicator());
                            }
                          });
                    } else {
                      return ListTile(
                        leading: Icon(Icons.error),
                        title: Text('No se encontró usuario'),
                      );
                    }
                  } else {
                    return Center(child: CircularProgressIndicator());
                  }
                },
              ),
            ),
            _menuInferior(),
          ],
        ),
      ),
    );
  }

  Widget _menuInferior() {
    return BottomNavigationBar(
      items: const <BottomNavigationBarItem>[
        BottomNavigationBarItem(
          icon: Icon(Icons.sports_tennis),
          label: 'Jugar',
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.school),
          label: 'Clases',
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.person),
          label: 'Perfil',
        ),
      ],
      currentIndex: _selectedIndex,
      onTap: (index) {
        setState(() {
          _selectedIndex = index;
        });
        switch (_selectedIndex) {
          case 0:
            Navigator.pushNamed(context, MenuPrincipal.ROUTE);
            break;
          case 1:
            Navigator.pushNamed(context, BonosList.ROUTE);
            break;
          case 2:
            Navigator.pushNamed(context, Perfil.ROUTE);
            break;
        }
      },
    );
  }
}
