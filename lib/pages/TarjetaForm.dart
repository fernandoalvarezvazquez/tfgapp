import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:tfgapp/api/peticiones.dart';
import 'package:tfgapp/clases/Usuario.dart';
import 'package:tfgapp/pages/TarjetaList.dart';

class TarjetaForm extends StatefulWidget{
  static const String ROUTE = "/new-tarjeta";

  @override
  _TarjetaForm createState() => _TarjetaForm();
}

class _TarjetaForm extends State<TarjetaForm>{

  final _formKey = GlobalKey<FormState>();
  String numero = '';
  String caducidad = '';
  String cvv = '';
  String titular = '';

  final numeroController = TextEditingController();
  final caducidadController = TextEditingController();

  late Usuario jugador;

  @override
  void dispose() {
    numeroController.dispose();
    caducidadController.dispose();
    super.dispose();
  }

  @override initState(){
    super.initState();
    getCurrentUser();
  }

  Future<Usuario> getCurrentUser() async{
    var peticiones = Peticiones();
    jugador = await peticiones.getUser();
    return await peticiones.getUser();
  }

  Future<void> addTarjeta(String titular, String numero, String caducidad, int jugadorId) async{
    var peticiones = Peticiones();
    await peticiones.createTarjeta(titular, numero, caducidad, jugadorId);
  }

  @override
  Widget build (BuildContext context){
    return Scaffold(
      appBar: AppBar(
        title: Text('Añadir tarjeta de credito'),
        backgroundColor: Colors.deepPurple,
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Form(
          key: _formKey,
          child: Column(
            children: [
              TextFormField(
                controller: numeroController,
                decoration: InputDecoration(
                  labelText: 'Número de tarjeta',
                  border: OutlineInputBorder(),
                ),
                keyboardType: TextInputType.number,
                inputFormatters: [LengthLimitingTextInputFormatter(19)],
                onChanged: (value) {
                  value = value.replaceAll(RegExp(r"\s+\b|\b\s"), ""); // Eliminar todos los espacios
                  if (value.length <= 16) {
                    StringBuffer newString = StringBuffer();
                    for (int i = 0; i < value.length; i++) {
                      if (i > 0 && i % 4 == 0) {
                        newString.write(" "); // Agrega un espacio cada 4 dígitos
                      }
                      newString.write(value[i]);
                    }
                    numeroController.text = newString.toString();
                    numeroController.selection = TextSelection.collapsed(offset: numeroController.text.length); // Mueve el cursor al final del texto
                  }
                  numero = numeroController.text;
                },
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Por favor ingresa el número de tarjeta';
                  }
                  return null;
                },
              ),
              SizedBox(height: 16),
              Row(
                children: [
                  Expanded(
                    child:TextFormField(
                      controller: caducidadController,
                      decoration: InputDecoration(
                        labelText: 'Caducidad (MM/AA)',
                        border: OutlineInputBorder(),
                      ),
                      keyboardType: TextInputType.number,
                      inputFormatters: [LengthLimitingTextInputFormatter(5)],
                      onChanged: (value) {
                        if (value.length == 2 && !value.contains("/")) {
                          caducidadController.text = value + "/";
                          caducidadController.selection = TextSelection.collapsed(offset: caducidadController.text.length); // Mueve el cursor al final del texto
                        }
                        caducidad = caducidadController.text;
                      },
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Por favor ingresa la fecha de vencimiento';
                        }
                        return null;
                      },
                    ),
                  ),
                  SizedBox(width: 10),
                  Expanded(
                    child: TextFormField(
                      decoration: InputDecoration(
                        labelText: 'CVV',
                        border: OutlineInputBorder(),
                      ),
                      keyboardType: TextInputType.number,
                      inputFormatters: [LengthLimitingTextInputFormatter(3)],
                      obscureText: true,
                      onChanged: (value) => cvv = value,
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Por favor ingresa el CVV';
                        }
                        return null;
                      },
                    ),
                  ),
                ],
              ),
              SizedBox(height: 16),
              TextFormField(
                decoration: InputDecoration(
                  labelText: 'Nombre del titular',
                  border: OutlineInputBorder(),
                ),
                onChanged: (value) => titular = value,
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Por favor ingresa el nombre del titular';
                  }
                  return null;
                },
              ),
              SizedBox(height: 16),
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                  backgroundColor: Colors.deepPurple,
                  foregroundColor: Colors.white,
                  padding: EdgeInsets.symmetric(horizontal: 30, vertical: 15),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                ),
                onPressed: () {
                  if (_formKey.currentState!.validate()) {
                    // Procesa los datos de la tarjeta
                    print('Tarjeta añadida!');
                    addTarjeta(titular, numero, caducidad, jugador.id);
                    Navigator.pushNamed(context, TarjetaList.ROUTE);
                  }
                },
                child: Text('Añadir Tarjeta'),
              ),
            ],
          ),
        ),
      ),
    );
  }
}