
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

class WebviewPaypal extends StatefulWidget{
  static const String ROUTE = "/webview-paypal";
  final String URLinicial;

  WebviewPaypal({required this.URLinicial});

  @override
  _WebviewPaypal createState() => _WebviewPaypal();
}

class _WebviewPaypal extends State<WebviewPaypal>{

  late WebViewController _controller;

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build (BuildContext context){

    _controller = WebViewController()
      ..setJavaScriptMode(JavaScriptMode.unrestricted)
      ..setNavigationDelegate(
        NavigationDelegate(
          onProgress: (int progress){

          },
          onPageStarted: (String url){},
          onPageFinished: (String url) {},
          onWebResourceError: (WebResourceError error) {},
          onNavigationRequest: (NavigationRequest request){
            if (request.url.startsWith("http://tuapp.com/cancelled")) {
              Future.delayed(Duration.zero, () {
                Navigator.pop(context, 'cancelled');
              });
              return NavigationDecision.prevent;
            } else if (request.url.startsWith("http://tuapp.com/confirmed")){
              print('Resques Correcto -----------------');
              Future.delayed(Duration.zero, () {
                Navigator.pop(context, 'completed');
              });
              return NavigationDecision.prevent;
            }
            return NavigationDecision.navigate;
          },
        ),
      )
      ..loadRequest(Uri.parse(widget.URLinicial));

    return Scaffold(
      appBar: AppBar(
        title: Text('Pago con Paypal'),
        backgroundColor: Colors.deepPurple,
      ),
      body: WebViewWidget( controller: _controller),
    );
  }
}