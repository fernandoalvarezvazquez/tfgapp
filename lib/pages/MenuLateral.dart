import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tfgapp/pages/BonoListPerfil.dart';
import 'package:tfgapp/pages/EditarPerfil.dart';
import 'package:tfgapp/pages/TarjetaList.dart';
import 'package:tfgapp/pages/inicio.dart';

import '../api/peticiones.dart';
import '../clases/Usuario.dart';

class MenuLateral extends StatelessWidget {

  late Future<Usuario> user;
  late Usuario usuario;

  @override
  void initState() {
    getCurrentUser();
  }

  Future<void> borrarToken() async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.remove('token');
  }

  Future<Usuario> getCurrentUser() async{
    var peticiones = Peticiones();
    user = peticiones.getUser();
    return await user;
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: [
          FutureBuilder<Usuario>(
            future: getCurrentUser(),
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                return CircularProgressIndicator();
              } else if (snapshot.hasError) {
                return Center(child: Text('Ha ocurrido un error.'));
              } else if (snapshot.hasData) {
                usuario = snapshot.data!;
                return UserAccountsDrawerHeader(
                  accountName: Text(usuario.login, style: TextStyle(
                      fontSize: 20.0, fontWeight: FontWeight.bold)),
                  accountEmail: Text('Nivel: '+usuario.nivel.toString(), style: TextStyle(fontSize: 16.0)),
                  /*currentAccountPicture: CircleAvatar(
                  backgroundImage: AssetImage('path/to/your/image'), // Puedes poner la imagen que quieras aquí.
                  backgroundColor: Colors.white,
                ),*/
                  decoration: BoxDecoration(
                    color: Colors.deepPurple,
                  ),
                );
              }
              else {
                return Center(child: Text('No hay datos disponibles.'));
              }
            }
          ),
          ListTile(
            leading: Icon(Icons.edit, color: Colors.deepPurple),
            title: Text('Editar Perfil', style: TextStyle(fontSize: 18.0)),
            onTap: () {
              Navigator.pushNamed(context, EditarPerfil.ROUTE);
            },
          ),
          Divider(
            height: 2.0,
            thickness: 2.0,
          ),
          ListTile(
            leading: Icon(Icons.card_giftcard, color: Colors.deepPurple),
            title: Text('Mis bonos', style: TextStyle(fontSize: 18.0)),
            onTap: () {
              Navigator.pushNamed(context, BonosListPerfil.ROUTE);
            },
          ),
          Divider(
            height: 2.0,
            thickness: 2.0,
          ),
          ListTile(
            leading: Icon(Icons.credit_card, color: Colors.deepPurple),
            title: Text('Mis tarjetas', style: TextStyle(fontSize: 18.0)),
            onTap: () {
              Navigator.pushNamed(context, TarjetaList.ROUTE);
            },
          ),
          Divider(
            height: 2.0,
            thickness: 2.0,
          ),
          ListTile(
            leading: Icon(Icons.monetization_on, color: Colors.deepPurple),
            title: Text('Mis transacciones', style: TextStyle(fontSize: 18.0)),
            onTap: () {
              Navigator.pushNamed(context, EditarPerfil.ROUTE);
            },
          ),
          Divider(
            height: 2.0,
            thickness: 2.0,
          ),
          ListTile(
            title: Text('Cerrar sesión', style: TextStyle(fontSize: 18.0, color: Colors.redAccent)),
            leading: Icon(Icons.logout, color: Colors.redAccent),
            onTap: () {
              //cerrar sesion
              borrarToken();
              Navigator.pushNamed(context, Inicio.ROUTE);
            },
          ),
        ],
      ),
    );
  }
}
