import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:tfgapp/pages/reservaDetail.dart';

import '../api/peticiones.dart';
import '../clases/Club.dart';
import '../clases/Partido.dart';
import '../clases/Pista.dart';
import '../clases/TimeSlot.dart';
import '../clases/Usuario.dart';

class clubDetail extends StatefulWidget{
  static const String ROUTE = "/club";
  final Club club;

  clubDetail({required this.club});

  @override
  _clubDetail createState() => _clubDetail();
}

class _clubDetail extends State<clubDetail>{

  TimeSlot? horaSeleccionada;
  late DateTime fechaSeleccionada;
  List<TimeSlot> franjasHorarias = [];
  List<DateTime> fechasDisponibles = [];

  late Future <List<Pista>> listaPistas;
  List<Pista> pistasDisponibles = [];
  late Future <List<Partido>> listaPartidos;
  List<Partido> partidosDisponibles = [];
  Map<Pista, List<DateTime>> filtroHorasIni = {};
  late Future<Usuario> user;

  @override
  void initState(){
    super.initState();

    // Establecer la fecha seleccionada a 'ahora' o al 'día siguiente' dependiendo de la hora
    DateTime ahora = DateTime.now();
    fechaSeleccionada = ahora.hour > 21 || (ahora.hour == 21 && ahora.minute >= 30)
        ? DateTime(ahora.year, ahora.month, ahora.day + 1)
        : DateTime(ahora.year, ahora.month, ahora.day);

    // Aquí no necesitamos preocuparnos de la hora, minuto, segundo, ya que ya estamos configurando la fecha correctamente.
    fechasDisponibles = List.generate(7, (index) {
      final fecha = DateTime.now().add(Duration(days: index));
      return DateTime(fecha.year, fecha.month, fecha.day);
    });

    actualizarFranjasHorarias();
  }

  /*Future<void> getImagenClub() async{
    http.Response? _imageResponse;
    try{
      var peticiones = Peticiones();
      var response = await peticiones.getImagenClub(widget.club.id);

      if (response != null) {
        setState(() {
          _imageResponse = response;
        });
      }
    } catch (e) {
      print("Error obteniendo imagen: $e");
    }
  }*/

  Future<Usuario> getCurrentUser() async{
    var peticiones = Peticiones();
    user = peticiones.getUser();
    return await user;
  }

  void reservarPista(Pista pista, double precio, DateTime hora_ini, DateTime hora_fin, Future<Usuario> creador){
    Navigator.push(context, MaterialPageRoute(builder: (context) => reservaDetail(pista: pista, precio: precio, hora_ini: hora_ini, hora_fin:hora_fin, creador: creador) ));
  }

  void actualizarFranjasHorarias() {
    franjasHorarias.clear();
    final ahora = DateTime.now();
    int horaInicio = ahora.day == fechaSeleccionada.day ? ahora.hour : 10;

    if (ahora.day == fechaSeleccionada.day) {
      if (ahora.minute > 30) {
        horaInicio += 1; // Incrementar la hora si ya pasaron 30 minutos
      }
    }

    for (int i = horaInicio; i <= 21; i++) {
      franjasHorarias.add(TimeSlot(i, 0));
      if (i != horaInicio || ahora.minute <= 30 || ahora.day != fechaSeleccionada.day) {
        franjasHorarias.add(TimeSlot(i, 30)); // Agregar la media hora excepto en la hora de inicio si ya pasaron 30 minutos
      }
    }

    if (fechaSeleccionada.day == ahora.day && ahora.minute > 0 && ahora.minute <= 30) {
      horaSeleccionada = TimeSlot(horaInicio, 30);
    } else {
      horaSeleccionada = franjasHorarias.first;
    }

    setState(() {});
  }

  Future<List<Pista>> obtenerPistas(int id) async{
    var peticiones = Peticiones();
    listaPistas = peticiones.pistasClub(id);
    return await listaPistas;
  }

  Future<List<Partido>> obtenerPartidos(int id) async{
    var peticiones = Peticiones();
    listaPartidos = peticiones.partidosClub(id);
    return await peticiones.partidosClub(id);
  }

  List<Pista> filtrarPistasDisponibles(List<Pista> pistasFut, List<Partido> reservasFut, DateTime horaSeleccionada) {
    List<Pista> pistas = pistasFut;
    List<Partido> reservas = reservasFut;
    List<Pista> pistasFiltradas = [];
    List<Pista> filtro = [];


    for (int i = 0; i<pistas.length; i++){
      filtroHorasIni[pistas[i]] = [];
      for (int j = 0; j<reservas.length; j++){
        if(reservas[j].idPista == pistas[i].id
            && (DateTime.parse(reservas[j].hora_ini).isBefore(horaSeleccionada) || DateTime.parse(reservas[j].hora_ini).isAtSameMomentAs(horaSeleccionada))
            && (DateTime.parse(reservas[j].hora_fin).isAfter(horaSeleccionada) )){
          filtro.add(pistas[i]);
        }
        if(reservas[j].idPista == pistas[i].id){
          filtroHorasIni[pistas[i]]?.add(DateTime.parse(reservas[j].hora_ini));
        }
      }
    }

    for (int j = 0; j<pistas.length; j++){
      if (!filtro.contains(pistas[j])){
        pistasFiltradas.add(pistas[j]);
      }
    }

    return pistasFiltradas;
  }

  Future<List<Pista>> obtenerDataCompleta(int clubId, DateTime fechaHoraSeleccionada) async {
    final pistas = await obtenerPistas(clubId);
    final partidos = await obtenerPartidos(clubId);
    return filtrarPistasDisponibles(pistas, partidos, fechaHoraSeleccionada);
  }

  int duracionMaximaDisponible(DateTime horaInicio, List<DateTime> horasInicioPartidos) {
    int duracion = 120;  // tiempo máximo (2 horas)
    for (var horaPartido in horasInicioPartidos) {
      if (horaInicio.isBefore(horaPartido)) {
        int diferencia = horaPartido.difference(horaInicio).inMinutes;
        if (diferencia < duracion) {
          duracion = diferencia;
        }
      }
    }
    return duracion;
  }

  Widget _pistaCard(Pista pista, DateTime fechaHoraSeleccionada) {
    int maxDuration = duracionMaximaDisponible(
        DateTime(fechaSeleccionada.year, fechaSeleccionada.month, fechaSeleccionada.day, horaSeleccionada!.hora, horaSeleccionada!.minuto),
        filtroHorasIni[pista] ?? []
    );

    print(maxDuration);

    return ExpansionTile(
      title: Text(pista.nombre),
      subtitle: Text(pista.precio.toString() + "€/hora"),
      children: <Widget>[
        Container(
          height: 100,
          child: ListView(
            scrollDirection: Axis.horizontal,
            children: [
              if (maxDuration >= 30)
                Card(
                  color: Colors.deepPurpleAccent[100],
                  child:Container(
                    width: 100,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: ListTile(
                        title: Text((pista.precio/2).toString()+'€'),
                        subtitle: Text('30min'),
                        onTap: (){
                          reservarPista(pista, pista.precio/2,
                              DateTime(fechaSeleccionada.year, fechaSeleccionada.month, fechaSeleccionada.day, horaSeleccionada!.hora, horaSeleccionada!.minuto),
                              DateTime(fechaSeleccionada.year, fechaSeleccionada.month, fechaSeleccionada.day, horaSeleccionada!.hora, horaSeleccionada!.minuto).add(Duration(minutes: 30)),
                              getCurrentUser()
                          );
                        },
                      ),
                    ),
                  ),
                ),
              if ((horaSeleccionada!.hora < 21 || (horaSeleccionada!.hora == 21 && horaSeleccionada!.minuto == 0)) && (maxDuration >= 60))
                Card(
                  color: Colors.deepPurpleAccent[100],
                  child:Container(
                    width: 100,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: ListTile(
                        title: Text((pista.precio).toString()+'€'),
                        subtitle: Text('60min'),
                        onTap: (){
                          reservarPista(pista, pista.precio,
                              DateTime(fechaSeleccionada.year, fechaSeleccionada.month, fechaSeleccionada.day, horaSeleccionada!.hora, horaSeleccionada!.minuto),
                              DateTime(fechaSeleccionada.year, fechaSeleccionada.month, fechaSeleccionada.day, horaSeleccionada!.hora, horaSeleccionada!.minuto).add(Duration(hours: 1)),
                              getCurrentUser());
                        },
                      ),
                    ),
                  ),
                ),
              if ((horaSeleccionada!.hora < 20 || (horaSeleccionada!.hora == 20 && horaSeleccionada!.minuto == 30)) && (maxDuration >= 90))
                Card(
                  color: Colors.deepPurpleAccent[100],
                  child:Container(
                    width: 100,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: ListTile(
                        title: Text((pista.precio*1.5).toString()+'€'),
                        subtitle: Text('90min'),
                        onTap: (){
                          reservarPista(pista, pista.precio*1.5,
                              DateTime(fechaSeleccionada.year, fechaSeleccionada.month, fechaSeleccionada.day, horaSeleccionada!.hora, horaSeleccionada!.minuto),
                              DateTime(fechaSeleccionada.year, fechaSeleccionada.month, fechaSeleccionada.day, horaSeleccionada!.hora, horaSeleccionada!.minuto).add(Duration(hours: 1, minutes: 30)),
                              getCurrentUser()
                          );
                        },
                      ),
                    ),
                  ),
                ),
              if ((horaSeleccionada!.hora < 20 || (horaSeleccionada!.hora == 20 && horaSeleccionada!.minuto == 0)) && (maxDuration >= 120))
                Card(
                  color: Colors.deepPurpleAccent[100],
                  child:Container(
                    width: 100,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: ListTile(
                        title: Text((pista.precio*2).toString()+'€'),
                        subtitle: Text('120min'),
                        onTap: (){
                          reservarPista(pista, pista.precio*2,
                              DateTime(fechaSeleccionada.year, fechaSeleccionada.month, fechaSeleccionada.day, horaSeleccionada!.hora, horaSeleccionada!.minuto),
                              DateTime(fechaSeleccionada.year, fechaSeleccionada.month, fechaSeleccionada.day, horaSeleccionada!.hora, horaSeleccionada!.minuto).add(Duration(hours: 2)),
                              getCurrentUser()
                          );
                        },
                      ),
                    ),
                  ),
                ),
            ],
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.deepPurple,
        title: (Text(widget.club.nombre)),
      ),
      body: Padding(
        padding: EdgeInsets.all(15.0),
        child: SingleChildScrollView(
          child: Column(
            children: [
              Card(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    if(widget.club.imagenPath.isNotEmpty)
                    AspectRatio(
                      aspectRatio: 16 / 6,
                      child: Image.network(
                        'http://10.0.2.2:8080/api/clubes/${widget.club.id}/imagen',
                        fit: BoxFit.fitWidth,
                        errorBuilder: (BuildContext context, Object exception, StackTrace? stackTrace) {
                          return Text('Error al cargar la imagen');
                        },
                      ),
                    ),
                    SizedBox(height: 15),
                    Center(
                      child: Text('Detalles del Club:',
                        style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
                      ),
                    ),
                    SizedBox(height: 15),
                    Row(
                      children: [
                        SizedBox(width: 5),
                        Icon(FontAwesomeIcons.house, size: 15),
                        SizedBox(width: 10),
                        Text('Nombre del Club: ${widget.club.nombre}', style: TextStyle(fontSize: 15.0)),
                      ],
                    ),
                    SizedBox(height: 10),
                    Row(
                      children: [
                        SizedBox(width: 5),
                        Icon(FontAwesomeIcons.diamondTurnRight, size: 15),
                        SizedBox(width: 10),
                        Text('Direccion: ${widget.club.direccion}', style: TextStyle(fontSize: 15.0)),
                      ],
                    ),
                    SizedBox(height: 10),
                    Row(
                      children: [
                        SizedBox(width: 5),
                        Icon(FontAwesomeIcons.envelopeOpenText, size: 15),
                        SizedBox(width: 10),
                        Text('Descripcion: ${widget.club.descripcion}', style: TextStyle(fontSize: 15.0)),
                      ],
                    ),
                    SizedBox(height: 10),
                    Row(
                      children: [
                        SizedBox(width: 5),
                        Icon(FontAwesomeIcons.envelopeOpenText, size: 15),
                        SizedBox(width: 10),
                        Text('Telefono: ${widget.club.telefono}', style: TextStyle(fontSize: 15.0)),
                      ],
                    ),
                    SizedBox(height: 10),
                    Row(
                      children: [
                        SizedBox(width: 5),
                        Icon(Icons.sports_tennis, size: 18),
                        SizedBox(width: 10),
                        Text('Numero de pistas: ${widget.club.num_pistas}', style: TextStyle(fontSize: 15.0)),
                      ],
                    ),
                    SizedBox(height: 10),
                  ],
                ),
              ),
              SizedBox(height: 20),
              ListTile(
                title: Text('Fecha seleccionada:'),
                trailing: DropdownButton<DateTime>(
                  value: fechaSeleccionada,
                  items: fechasDisponibles.map((DateTime fecha) {
                    return DropdownMenuItem<DateTime>(
                      value: fecha,
                      child: Text('${fecha.day}/${fecha.month}/${fecha.year}'),
                    );
                  }).toList(),
                  onChanged: (DateTime? nuevaFecha) {
                    if (nuevaFecha != null) {
                      setState(() {
                        fechaSeleccionada = nuevaFecha;
                        actualizarFranjasHorarias();
                      });
                    }
                  },
                ),
              ),
              ListTile(
                title: Text('Hora seleccionada: '),
                trailing: DropdownButton<TimeSlot>(
                  value: horaSeleccionada,
                  items: franjasHorarias.map((TimeSlot slot) {
                    return DropdownMenuItem<TimeSlot>(
                      value: slot,
                      child: Text(slot.toString()),
                    );
                  }).toList(),
                  onChanged: (TimeSlot? nuevaHora) {
                    setState(() {
                      horaSeleccionada = nuevaHora;
                    });
                  },
                ),
              ),
              FutureBuilder<List<Pista>>(
                  future: obtenerDataCompleta(widget.club.id,
                      DateTime(fechaSeleccionada.year, fechaSeleccionada.month, fechaSeleccionada.day,horaSeleccionada!.hora, horaSeleccionada!.minuto)),
                  builder: (context, snapshot) {
                    if (snapshot.connectionState == ConnectionState.waiting) {
                      return CircularProgressIndicator(); // Muestra un indicador de carga mientras espera
                    } else if (snapshot.hasError) {
                      return Text('Error: ${snapshot.error}'); // Manejar errores
                    } else {
                      // Construir la lista de pistas una vez que los datos estén disponibles
                      final pistas = snapshot.data ?? [];
                      return Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: SingleChildScrollView(
                            child: Column(
                              children: [
                                Text('Pistas:', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),),
                                Column(
                                  children: pistas.map((pista) => _pistaCard(pista, fechaSeleccionada)).toList(),
                                ),
                              ],
                            )

                        ),
                      );
                    }
                  },
                ),
            ],
          ),
        ),
      ),
    );
  }
}