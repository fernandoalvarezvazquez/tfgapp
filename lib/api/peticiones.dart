import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tfgapp/clases/BonoClases.dart';
import 'package:tfgapp/clases/Club.dart';
import 'package:tfgapp/clases/Tarjeta.dart';
import 'package:tfgapp/clases/Usuario.dart';

import '../clases/Partido.dart';
import '../clases/Pista.dart';

class Peticiones{

  String ip = "10.0.2.2";

  Future<String?> getToken() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString('token');
  }

  // POST /api/authenticate             -----------------------------------
  Future<String> autenticar(String login, String password) async {

    var data = {
      "login": login,
      "password": password,
    };

    var url = 'http://$ip:8080/api/authenticate';

    var response = await http.post(
      Uri.parse(url),
      body: jsonEncode(data),
      headers: {"Content-Type": "application/json"},
    );

    if (response.statusCode == 200) {
      // Si la respuesta es exitosa, decodifica el cuerpo
      var jsonResponse = jsonDecode(response.body);

      // Extrae el token
      String token = jsonResponse['token'];

      if (token != null) {
        SharedPreferences prefs = await SharedPreferences.getInstance();
        print('Token: ' + token);
        await prefs.setString('token', token);
        return token;
      } else {
        throw Exception('Token no encontrado en la respuesta');
      }
    } else if(response.statusCode == 401){
      throw Exception('Usuario o contraseña incorrectos.');

    } else {
      throw Exception('Error al autenticar el usuario');
    }
  }

  // GET /api/account           ---------------------------------
  Future<Usuario> getUser() async{
    Usuario user;
    final token = await getToken();
    var url = Uri.parse("http://$ip:8080/api/account");

    final response = await http.get(
        url,
        headers: {
          'Authorization': 'Bearer $token',
        },
    );

    print('Token(getUser()): ' + token!);

    if (response.statusCode == 200){
      String body = utf8.decode(response.bodyBytes);

      final jsonData = jsonDecode(body);
      user = Usuario.fromJson(jsonData);
      return user;

    } else{
      throw Exception('Error al recuperar el usuario');
    }
  }

  // POST /api/register       --------------------------------------
  Future<void> registerUser(String login, String contra) async{
    var data = {
      "login": login,
      "contrasena": contra,
    };

    var url = 'http://$ip:8080/api/register';

    var response = await http.post(
      Uri.parse(url),
      body: jsonEncode(data),
      headers: {"Content-Type": "application/json"},
    );

    if (response.statusCode == 200) {
      print('200 OK');
    } else {
      throw Exception('Error al registrar el usuario');
    }

  }

  // GET /api/usuarios      --------------------------------------
  Future<List<Usuario>> usuarios() async{
    List<Usuario> users = [];
    var url = Uri.parse("http://$ip:8080/api/usuarios");
    final response = await http.get(
        url
    );

    if (response.statusCode == 200){
      String body = utf8.decode(response.bodyBytes);

      final jsonData = jsonDecode(body);

      for (var item in jsonData){
        if (item['fotoPerfil'] == null){
          users.add(Usuario(id: item['id'], login: item['login'], authority: item['authority'], nivel: item['nivel'],  fotoPerfil: ""));
        } else
          users.add(Usuario(id: item['id'], login: item['login'], authority: item['authority'], nivel: item['nivel'],  fotoPerfil: item['fotoPerfil']));
      }
      return users;

    } else{
      throw Exception('Error al recuperar los clubes');
    }
  }

  // GET /api/pistas/:id    -------------------------------------------
  Future<Pista> getPista(int id) async{
    late Pista pista;

    var url = Uri.parse("http://$ip:8080/api/pistas/$id");
    final response = await http.get(
        url
    );

    if (response.statusCode == 200){
      String body = utf8.decode(response.bodyBytes);

      final jsonData = jsonDecode(body);

      pista = Pista.fromJson(jsonData);

      return pista;

    } else{
      throw Exception('Error al recuperar la pista');
    }
  }

  // GET /api/usuarios      --------------------------------------
  Future<List<Usuario>> jugadores() async{
    List<Usuario> users = [];
    var url = Uri.parse("http://$ip:8080/api/usuarios");
    final response = await http.get(
        url
    );

    if (response.statusCode == 200){
      String body = utf8.decode(response.bodyBytes);

      final jsonData = jsonDecode(body);

      for (var item in jsonData){
        if(item['authority'] == 'PLAYER')
          if (item['fotoPerfil'] == null){
            users.add(Usuario(id: item['id'], login: item['login'], authority: item['authority'], nivel: item['nivel'], fotoPerfil: ""));
          } else
            users.add(Usuario(id: item['id'], login: item['login'], authority: item['authority'], nivel: item['nivel'], fotoPerfil: item['fotoPerfil']));
      }
      return users;

    } else{
      throw Exception('Error al recuperar los usuarios');
    }
  }

  // GET /api/usuarios/:id    -------------------------------------------
  Future<Usuario> getUsuario(int id) async{
    late Usuario user;

    var url = Uri.parse("http://$ip:8080/api/usuarios/$id");
    final response = await http.get(
        url
    );

    if (response.statusCode == 200){
      String body = utf8.decode(response.bodyBytes);

      final jsonData = jsonDecode(body);

      user = Usuario.fromJson(jsonData);

      return user;

    } else{
      throw Exception('Error al recuperar el usuario');
    }
  }

  // GET /api/usuarios/perfil?login=:login    -------------------------------------------
  Future<Usuario> getUsuarioByLogin(String login) async{
    late Usuario user;

    var url = Uri.parse("http://$ip:8080/api/usuarios/perfil?login=$login");
    final response = await http.get(
        url
    );

    if (response.statusCode == 200){
      String body = utf8.decode(response.bodyBytes);

      final jsonData = jsonDecode(body);

      user = Usuario.fromJson(jsonData);

      return user;

    } else{
      throw Exception('Error al recuperar el usuario');
    }
  }

  // PUT /api/usuarios/id       -------------------------------------
  Future<void> updateUser(int id, Usuario user) async{
    var url = Uri.parse("http://$ip:8080/api/usuarios/$id");
    var data = {
      "id": user.id,
      "login": user.login,
      "contrasena": user.contrasena,
      "authority": user.authority,
      "nivel": user.nivel,
      "fotoPerfil": user.fotoPerfil,
    };
    final token = await getToken();

    final response = await http.put(
      url,
      body: jsonEncode(data),
      headers: {
        'Authorization': 'Bearer $token',
        "Content-Type": "application/json",
      },
    );

    if (response.statusCode == 200){
      print("200 OK");
    } else{
      throw Exception('Error al actualizar el usuario');
    }

  }

  // PUT /api/usuarios/id       -------------------------------------
  Future<void> updateFotoUser(Usuario user, String path) async{

    var url = Uri.parse("http://$ip:8080/api/usuarios/cargar-imagen");
    final token = await getToken();

    var request = http.MultipartRequest('PUT', url);

    request.headers.addAll({"Authorization": "Bearer $token"});

    request.files.add(await http.MultipartFile.fromPath("imagenArchivo", path));

    request.fields['id'] = user.id.toString();
    request.fields['login'] = user.login;
    request.fields['autoridad'] = user.authority;
    request.fields['nivel'] = user.nivel.toString();

    final response = await request.send();

    if (response.statusCode == 200){
      print("200 OK carga-imagen");
    } else{
      throw Exception('Error al actualizar la foto del usuario');
    }

  }

  // PUT /api/usuarios/id/nivel      -------------------------------------
  Future<void> updateUserNivel(Usuario user, double nivel) async{
    var url = Uri.parse("http://$ip:8080/api/usuarios/${user.id}/nivel");
    var data = {
      "id": user.id,
      "nivel": nivel,
    };
    final token = await getToken();

    final response = await http.put(
      url,
      body: jsonEncode(data),
      headers: {
        'Authorization': 'Bearer $token',
        "Content-Type": "application/json",
      },
    );

    if (response.statusCode == 200){
      print("200 OK");
    } else{
      throw Exception('Error al actualizar el nivel de usuario');
    }

  }

  // GET /api/usuarios/:id/partidos -------------------------------
  Future<List<Partido>> getPartidosJugadosUser(int id) async{
    List<Partido> partidos = [];
    var url = Uri.parse("http://$ip:8080/api/usuarios/$id/partidos");
    DateTime horaActual = DateTime.now();

    final response = await http.get(
        url
    );

    if (response.statusCode == 200){
      print('200 OK /api/usuarios/:id/partidos');
      String body = utf8.decode(response.bodyBytes);

      final jsonData = jsonDecode(body);

      for (var item in jsonData){
        if(DateTime.parse(item['hora_fin']).isBefore(horaActual) )
          if(item['resultado'] == null)
            partidos.add(Partido(item['id'], item['creador'], item['hora_ini'], item['hora_fin'], "", "",
                List<String>.from(item['jugadores'].map((x) => x.toString())), item['pagado'], item['precioFinal'], item['pista']));
          else
            partidos.add(Partido(item['id'], item['creador'], item['hora_ini'], item['hora_fin'], item['resultado'], item['resultadoSets'],
                List<String>.from(item['jugadores'].map((x) => x.toString())), item['pagado'], item['precioFinal'], item['pista']));
      }

      return partidos;

    } else{
      throw Exception('Error al recuperar los partidos');
    }

  }

  // GET /api/usuarios/:id/partidos -------------------------------
  Future<List<Partido>> getPartidosFuturosUser(int id) async{
    List<Partido> partidos = [];
    var url = Uri.parse("http://$ip:8080/api/usuarios/$id/partidos");
    DateTime horaActual = DateTime.now();

    final response = await http.get(
        url
    );

    if (response.statusCode == 200){
      print('200 OK /api/usuarios/:id/partidos');
      String body = utf8.decode(response.bodyBytes);

      final jsonData = jsonDecode(body);

      for (var item in jsonData){
        if(DateTime.parse(item['hora_ini']).isAfter(horaActual) )
          if(item['resultado'] == null)
            partidos.add(Partido(item['id'], item['creador'], item['hora_ini'], item['hora_fin'], "", "",
                List<String>.from(item['jugadores'].map((x) => x.toString())), item['pagado'], item['precioFinal'], item['pista']));
          else
            partidos.add(Partido(item['id'], item['creador'], item['hora_ini'], item['hora_fin'], item['resultado'], item['resultadoSets'],
                List<String>.from(item['jugadores'].map((x) => x.toString())), item['pagado'], item['precioFinal'], item['pista']));
      }

      return partidos;

    } else{
      throw Exception('Error al recuperar los partidos futuros');
    }

  }

  // GET /api/clubes              ------------------------------
  Future<List<Club>> clubes() async{
    List<Club> clubes = [];
    var url = Uri.parse("http://$ip:8080/api/clubes");
    final response = await http.get(
        url
    );

    if (response.statusCode == 200){
      String body = utf8.decode(response.bodyBytes);

      final jsonData = jsonDecode(body);

      for (var item in jsonData){
        if(item['adminClub'] != null)
          clubes.add(Club(item['id'], item['nombre'], item['direccion'], item['descripcion'], item['num_pistas'], item['telefono'], item['adminClub'], item['imagenPath']));
      }
      return clubes;

    } else{
      throw Exception('Error al recuperar los clubes');
    }

  }

  //GET /api/clubes/:id/imagen
  Future<http.Response> getImagenClub(int id) async{
    http.Response? _imageResponse;

    var url = Uri.parse("http://$ip:8080/api/clubes/${id}/imagen");
    final response = await http.get(
        url
    );

    if (response.statusCode == 200){
      _imageResponse = response;
      return _imageResponse;

    } else{
      throw Exception('Error al recuperar la imagen del club');
    }
  }

  // Get /api/pistas  params: id(CLUB)      ------------------------------
  Future<List<Pista>> pistasClub(int idClub) async{
    List<Pista> pistas = [];

    var url = Uri.parse("http://$ip:8080/api/pistas?id=$idClub");
    final response = await http.get(
      url
    );

    if (response.statusCode == 200){
      String body = utf8.decode(response.bodyBytes);

      final jsonData = jsonDecode(body);

      for (var item in jsonData){
        pistas.add(Pista(item['id'], item['nombre'], item['estado'], item['precio'], item['tipoPista'], item['club']));
      }
      return pistas;

    } else{
      throw Exception('Error al recuperar las pistas');
    }
  }

  // GET /api/clubes/:id     --------------------------------------
  Future<Club> getClub(int id) async{
    late Club club;

    var url = Uri.parse("http://$ip:8080/api/clubes/$id");
    final response = await http.get(
        url
    );

    if (response.statusCode == 200){
      String body = utf8.decode(response.bodyBytes);

      final jsonData = jsonDecode(body);

      club = Club.fromJson(jsonData);

      return club;

    } else{
      throw Exception('Error al recuperar el club');
    }
  }

  //GET /api/partidos/:id
  Future<Partido> getPartido(int id) async {
    late Partido partido;

    var url = Uri.parse("http://$ip:8080/api/partidos/$id");
    final response = await http.get(
        url
    );

    if (response.statusCode == 200){
      String body = utf8.decode(response.bodyBytes);

      final jsonData = jsonDecode(body);

      partido = Partido.fromJson(jsonData);

      return partido;

    } else{
      throw Exception('Error al recuperar el partido');
    }
  }

  //GET /api/partidos/find?idPista=....&hora=...
  Future<Partido> getPartidoPistaHora(int idPista, String hora) async {
    late Partido partido;

    var url = Uri.parse("http://$ip:8080/api/partidos/find?idPista=$idPista&hora=$hora");
    final response = await http.get(
        url
    );

    if (response.statusCode == 200){
      String body = utf8.decode(response.bodyBytes);

      final jsonData = jsonDecode(body);

      partido = Partido.fromJson(jsonData);

      return partido;

    } else{
      throw Exception('Error al recuperar el partido');
    }
  }

  // GET /api/partidos/club/id          --------------------------------------
  Future<List<Partido>> partidosClub(int idClub) async{
    List<Partido> partidos = [];

    var url = Uri.parse("http://$ip:8080/api/partidos/club/$idClub");
    final response = await http.get(
        url
    );

    if (response.statusCode == 200){
      String body = utf8.decode(response.bodyBytes);

      final jsonData = jsonDecode(body);

      for (var item in jsonData){
        partidos.add(Partido(item['id'], item['creador'], item['hora_ini'], item['hora_fin'], "", "",
            List<String>.from(item['jugadores'].map((x) => x.toString())), item['pagado'], item['precioFinal'], item['pista']));
      }
      return partidos;

    } else{
      throw Exception('Error al recuperar los partidos');
    }

  }

  // PUT /api/partido/:id/resultado     ---------------------------------------
  Future<void> updateResultado(int id, Partido partido) async {
    var url = Uri.parse("http://$ip:8080/api/partidos/$id/resultado");
    var data = {
      "id": partido.id,
      "resultado": partido.resultado,
      "resultadoSets": partido.resultadoSets,
    };
    final token = await getToken();

    final response = await http.put(
      url,
      body: jsonEncode(data),
      headers: {
        'Authorization': 'Bearer $token',
        "Content-Type": "application/json",
      },
    );

    if (response.statusCode == 200) {
      print("200 OK /partidos/resultado");
    } else {
      throw Exception('Error al actualizar el resultado');
    }
  }

  // PUT /api/partido/:id/pagado       ------------------------------------
  Future<void> updatePagado(int id, Partido partido) async {
    var url = Uri.parse("http://$ip:8080/api/partidos/$id/pagado");
    var data = {
      "id": partido.id,
      "pagado": partido.pagado,
    };
    final token = await getToken();

    final response = await http.put(
      url,
      body: jsonEncode(data),
      headers: {
        'Authorization': 'Bearer $token',
        "Content-Type": "application/json",
      },
    );

    if (response.statusCode == 200) {
      print("200 OK /partidos/pagado");
    } else {
      throw Exception('Error al actualizar el pago');
    }
  }

  // PUT /api/partido/:id/add-jugador       ------------------------------------
  Future<void> updatePartidoJugador(Partido partido, Usuario jugador) async {
    var url = Uri.parse("http://$ip:8080/api/partidos/${partido.id}/add-jugador?idJugador=${jugador.id}");
    var data = {
      "id": partido.id
    };
    final token = await getToken();

    final response = await http.put(
      url,
      body: jsonEncode(data),
      headers: {
        'Authorization': 'Bearer $token',
        "Content-Type": "application/json",
      },
    );

    if (response.statusCode == 200) {
      print("200 OK /partidos/add-jugador");
    } else {
      throw Exception('Error al actualizar el partido/jugador');
    }
  }

  // PUT /api/partido/:id/delete-jugador       ------------------------------------
  Future<void> updatePartidoJugadorDelete(Partido partido, Usuario jugador) async {
    var url = Uri.parse("http://$ip:8080/api/partidos/${partido.id}/delete-jugador?idJugador=${jugador.id}");
    var data = {
      "id": partido.id
    };
    final token = await getToken();

    final response = await http.put(
      url,
      body: jsonEncode(data),
      headers: {
        'Authorization': 'Bearer $token',
        "Content-Type": "application/json",
      },
    );

    if (response.statusCode == 200) {
      print("200 OK /partidos/add-jugador");
    } else {
      throw Exception('Error al actualizar el partido/jugador');
    }
  }

  // POST /api/partidos     --------------------------------------
  Future<void> createPartido(BuildContext context, String creador, DateTime hora_ini, DateTime hora_fin, int idPista, bool pagado) async{
    var data = {
      "creador": creador,
      "hora_ini": hora_ini.toIso8601String(),
      "hora_fin": hora_fin.toIso8601String(),
      "resultado": "",
      "resultadoSets": "",
      "pagado": pagado,
      "jugadores": [],
      "pista": idPista
    };

    final token = await getToken();

    var url = 'http://$ip:8080/api/partidos';

    var response = await http.post(
      Uri.parse(url),
      body: jsonEncode(data),
      headers: {
        "Content-Type": "application/json",
        'Authorization': 'Bearer $token',
      },
    );

    if (response.statusCode == 200) {
      print('200 OK /api/partidos');
    }
    else {
      print('Muestra el snackbar');
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text('No se ha podido reservar la pista a esta hora'),
          backgroundColor: Colors.red,
          duration: Duration(seconds: 3),
        )
      );

    }
  }

  //DELETE /api/tarjetas/id    --------------------------------------
  Future<void> deleteTarjeta(int id) async {
    final token = await getToken();

    var url = 'http://$ip:8080/api/tarjetas/$id';

    var response = await http.delete(
      Uri.parse(url),
      headers: {
        "Content-Type": "application/json",
        'Authorization': 'Bearer $token',
      },
    );

    if (response.statusCode == 200) {
      print('200 OK /api/tarjetas (delete)');
    } else {
      throw Exception('Error al borrar tarjeta');
    }
  }

  // GET /api/tarjetas ------------------------------------
  Future<List<Tarjeta>> getTarjetas(int id) async {
    List<Tarjeta> tarjetas = [];
    final token = await getToken();

    var url = Uri.parse("http://$ip:8080/api/tarjetas/usuarios/$id");
    final response = await http.get(
        url,
      headers: {
        "Content-Type": "application/json",
        'Authorization': 'Bearer $token',
      },
    );

    if (response.statusCode == 200){
      String body = utf8.decode(response.bodyBytes);

      final jsonData = jsonDecode(body);

      for (var item in jsonData){
        tarjetas.add(Tarjeta(item['id'], item['titular'], item['numero'], /*item['ultimos4numeros'],*/ item['caducidad'], item['jugadorId']));
      }
      return tarjetas;

    } else{
      throw Exception('Error al recuperar las tarjetas');
    }
  }

  // POST /api/tarjetas     --------------------------------------
  Future<void> createTarjeta(String titular, String numero, String caducidad, int jugadorId) async{
    var data = {
      "titular": titular,
      "numero": numero,
      "caducidad": caducidad,
      "jugadorId": jugadorId
    };

    final token = await getToken();

    var url = 'http://$ip:8080/api/tarjetas';

    var response = await http.post(
      Uri.parse(url),
      body: jsonEncode(data),
      headers: {
        "Content-Type": "application/json",
        'Authorization': 'Bearer $token',
      },
    );

    if (response.statusCode == 200) {
      print('200 OK /api/tarjetas');
    } else {
      throw Exception('Error al generar tarjeta');
    }
  }

  //DELETE /api/tarjetas/id    --------------------------------------
  Future<void> deletePartido(int id) async {
    final token = await getToken();

    var url = 'http://$ip:8080/api/partidos/$id';

    var response = await http.delete(
      Uri.parse(url),
      headers: {
        "Content-Type": "application/json",
        'Authorization': 'Bearer $token',
      },
    );

    if (response.statusCode == 200) {
      print('200 OK /api/partidos (delete)');
    } else {
      throw Exception('Error al borrar partido');
    }
  }

  //POST /api/transacciones/iniciarPago     -----------------------------
  Future<String> inicarPago(double precio) async{
    String urlPaypal = "";
    final token = await getToken();

    var url = 'http://$ip:8080/api/transacciones/iniciarPago?precio=$precio';
    var response = await http.post(
      Uri.parse(url),
      headers: {
        //"Content-Type": "application/json",
        'Authorization': 'Bearer $token',
      },
    );
    urlPaypal = response.body.trim();
    print(urlPaypal);

    if (response.statusCode == 200) {
      print('200 OK /api/transaccion/iniciarPago');
      return urlPaypal;
    } else {
      throw Exception('Error al generar transaccion');
    }
  }

  //GET /api/bonos
  Future<List<BonoClases>> getBonos() async{
    List<BonoClases> bonos = [];

    var url = Uri.parse("http://$ip:8080/api/bonos");
    final response = await http.get(
      url,
      headers: {
        "Content-Type": "application/json",
      },
    );

    if (response.statusCode == 200){
      String body = utf8.decode(response.bodyBytes);

      final jsonData = jsonDecode(body);

      for (var item in jsonData){
        if(item['idUser'] == null)
          bonos.add(BonoClases(item['id'], item['nombre'], item['descripcion'], item['precio'], item['num_clases'], item['idClub'], 0));
        else bonos.add(BonoClases(item['id'], item['nombre'], item['descripcion'], item['precio'], item['num_clases'], item['idClub'], item['idUser']));
      }
      return bonos;

    } else{
      throw Exception('Error al recuperar los bonos');
    }
  }

  // PUT /api/bonos/:id  Pagar      ------------------------------------
  Future<void> updateBonoPagado(BonoClases bono, int idJugador) async {
    var url = Uri.parse("http://$ip:8080/api/bonos/${bono.id}/comprador");
    var data = {
      "id": bono.id,
      "idUser": idJugador,
    };
    final token = await getToken();

    final response = await http.put(
      url,
      body: jsonEncode(data),
      headers: {
        'Authorization': 'Bearer $token',
        "Content-Type": "application/json",
      },
    );

    if (response.statusCode == 200) {
      print("200 OK /partidos/add-jugador");
    } else {
      throw Exception('Error al actualizar el bono/pagado');
    }
  }

}