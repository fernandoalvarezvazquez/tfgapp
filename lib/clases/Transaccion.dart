
enum PaymentStatus {
  PENDIENTE,
  COMPLETADO,
  FALLIDA,
  REINGRESADO
}

class Transaccion{

  int id = 0;
  String orderId = "";
  double cantidad = 0.0;
  String moneda = "";
  DateTime fecha = DateTime.now();
  PaymentStatus status = PaymentStatus.PENDIENTE;

  Transaccion(id, orderId, cantidad, moneda, fecha, status){
    this.id = id;
    this.orderId = orderId;
    this.cantidad = cantidad;
    this.moneda = moneda;
    this.fecha = fecha;
    this.status = status;
  }

}