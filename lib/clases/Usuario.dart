import 'dart:ffi';

class Usuario{

  int id = 0;
  String login = "";
  String? contrasena;
  String authority = "";
  double nivel = 0.0;
  String fotoPerfil = "";

  Usuario({required this.id, required this.login, this.contrasena, required this.authority, required this.nivel, required this.fotoPerfil});

  Usuario.fromJson(Map<String, dynamic> json){
    id = json['id'];
    login = json['login'];
    contrasena = json['contrasena'];
    authority = json['authority'];
    nivel = json['nivel'];
    fotoPerfil = json['fotoPerfil'];
  }
}