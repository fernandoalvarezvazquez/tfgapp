class TimeSlot {
  final int hora;
  final int minuto;

  TimeSlot(this.hora, this.minuto);

  @override
  String toString() {
    return '$hora:${minuto.toString().padLeft(2, '0')}';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is TimeSlot && other.hora == hora && other.minuto == minuto;
  }

  @override
  int get hashCode => hora.hashCode ^ minuto.hashCode;
}
