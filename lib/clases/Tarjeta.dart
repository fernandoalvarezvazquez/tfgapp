
class Tarjeta{

  int id = 0;
  String numero = "";
  //String ultimos4numeros = "";
  String titular = "";
  String caducidad = "";
  int jugadorId = 0;

  Tarjeta(id, titular, numero, caducidad, jugadorId){
    this.id = id;
    this.numero = numero;
    //this.ultimos4numeros = ultimos4numeros;
    this.titular = titular;
    this.caducidad = caducidad;
    this.jugadorId = jugadorId;
  }
}