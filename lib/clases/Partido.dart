

class Partido{
  int id = 0;
  String creador = "";
  String hora_ini = "";
  String hora_fin = "";
  String resultado = "";
  String resultadoSets = "";
  List<String> jugadores = [];
  bool pagado = false;
  double precioFinal = 0;
  int idPista = 0;

  Partido(id, creador, hora_ini, hora_fin, resultado, resultadoSets, jugadores, pagado, precioFinal, idPista){
    this.id = id;
    this.creador= creador;
    this.hora_ini = hora_ini;
    this.hora_fin = hora_fin;
    this.resultado = resultado;
    this.resultadoSets = resultadoSets;
    this.jugadores = jugadores;
    this.pagado = pagado;
    this.precioFinal = precioFinal;
    this.idPista = idPista;
  }

  Partido.fromJson(Map<String, dynamic> json){
    id = json['id'];
    creador = json['creador'];
    hora_ini = json['hora_ini'];
    hora_fin = json['hora_fin'];
    resultado = json['resultado'];
    resultadoSets = json['resultadoSets'];
    pagado = json['pagado'];
    precioFinal = json['precioFinal'];
    jugadores = (json['jugadores'] as List).map((e) => e.toString()).toList();
    idPista = json['pista'];
  }

}