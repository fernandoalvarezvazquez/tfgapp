
class BonoClases{
  int id = 0;
  String nombre = "";
  String descripcion = "";
  double precio = 0;
  int num_clases = 0;
  int idClub = 0;
  int idComprador = 0;

  BonoClases(id, nombre, descripcion, precio, num_clases, idClub, idComprador){
    this.id = id;
    this.nombre = nombre;
    this.descripcion = descripcion;
    this.precio = precio;
    this.num_clases = num_clases;
    this.idClub = idClub;
    this.idComprador = idComprador;
  }

  BonoClases.fromJson(Map<String, dynamic> json){
    id = json['id'];
    nombre = json['nombre'];
    descripcion = json['descripcion'];
    precio = json['precio'];
    num_clases = json['num_clases'];
    idClub = json['idClub'];
    idComprador = json['idUser'];
  }
}