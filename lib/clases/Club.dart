
class Club{

  int id = 0;
  String nombre = "";
  String direccion = "";
  String descripcion = "";
  String telefono = "";
  int num_pistas = 0;
  String adminClub = "";
  String imagenPath = "";

  Club(id, nombre, direccion, descripcion, num_pistas, telefono, adminClub, imagenPath){
    this.id = id;
    this.nombre = nombre;
    this.direccion = direccion;
    this.descripcion = descripcion;
    this.telefono = telefono;
    this.num_pistas = num_pistas;
    this.adminClub = adminClub;
    this.imagenPath = imagenPath;
  }

  Club.fromJson(Map<String, dynamic> json){
    id = json['id'];
    nombre = json['nombre'];
    direccion = json['direccion'];
    descripcion = json['descripcion'];
    telefono = json['telefono'];
    num_pistas = json['num_pistas'];
    adminClub = json['adminClub'];
    imagenPath = json['imagenPath'];
  }
}
