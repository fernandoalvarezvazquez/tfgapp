
import 'dart:ffi';

class Pista{
  int id = 0;
  String nombre = "";
  bool estado = false;
  double precio = 0;
  String tipoPista = "";
  int club = 0;

  Pista(id, nombre, estado, precio, tipoPista, club){
    this.id = id;
    this.nombre = nombre;
    this.estado = estado;
    this.precio = precio;
    this.tipoPista = tipoPista;
    this.club = club;
  }

  Pista.fromJson(Map<String, dynamic> json){
    id = json['id'];
    nombre = json['nombre'];
    estado = json['estado'];
    precio = json['precio'];
    tipoPista = json['tipoPista'];
    club = json['club'];
  }
}