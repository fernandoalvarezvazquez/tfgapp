
import 'package:flutter/material.dart';
import 'package:tfgapp/pages/AuntenticacionPage.dart';
import 'package:tfgapp/pages/BonoListPerfil.dart';
import 'package:tfgapp/pages/BonosList.dart';
import 'package:tfgapp/pages/EditarPerfil.dart';
import 'package:tfgapp/pages/FormRegister.dart';
import 'package:tfgapp/pages/MostrarClubes.dart';
import 'package:tfgapp/pages/MenuPrincipal.dart';
import 'package:tfgapp/pages/Perfil.dart';
import 'package:tfgapp/pages/RegisterPage.dart';
import 'package:tfgapp/pages/TarjetaForm.dart';
import 'package:tfgapp/pages/TarjetaList.dart';
import 'package:tfgapp/pages/inicio.dart';
import 'package:tfgapp/pages/reservarPista.dart';

void main() async{
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        initialRoute: Inicio.ROUTE,
        routes: {
          Inicio.ROUTE : (BuildContext context) => Inicio(),
          AutenticacionPage.ROUTE : (BuildContext context) => AutenticacionPage(),
          MenuPrincipal.ROUTE : (BuildContext context) => MenuPrincipal(),
          MostrarClubes.ROUTE : (BuildContext context) => MostrarClubes(),
          reservarPista.ROUTE : (BuildContext context) => reservarPista(),
          RegisterPage.ROUTE : (BuildContext context) => RegisterPage(),
          FormRegister.ROUTE : (BuildContext context) => FormRegister(),
          Perfil.ROUTE : (BuildContext context) => Perfil(),
          EditarPerfil.ROUTE : (BuildContext context) => EditarPerfil(),
          TarjetaList.ROUTE : (BuildContext context) => TarjetaList(),
          TarjetaForm.ROUTE : (BuildContext context) => TarjetaForm(),
          BonosList.ROUTE : (BuildContext context) => BonosList(),
          BonosListPerfil.ROUTE : (BuildContext context) => BonosListPerfil(),
        }
    );
  }

}